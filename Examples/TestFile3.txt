int main(x, y, operationNumber){
    print("You entered these numbers: " + x + ", "+ y);
    if( operationNumber == 0 )
    {
        print("Result of addition: " + (x + y));
    }
    elif( operationNumber == 1 )
    {
        print("Result of subtraction: " + x-y);
    }
    elif( operationNumber == 2 )
    {
        print("Result of multiplication: " + x*y);
    }
    elif( operationNumber == 3 )
    {
        print("Result of division: " + x/y);
    }
    elif( operationNumber == 4 )
    {
        print("Result of modulo: " + x%y);
    }
    return 0;
}
