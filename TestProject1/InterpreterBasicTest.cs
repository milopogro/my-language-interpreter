using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TKOM;

namespace TKOMTest
{
    public static class InterpreterTestTools
    {
        public static (Variant, IList<ReadableException>, IList<string>)  GetResult(string args, string app)
        {
            var printer = new PrinterMock();
            var errorHandler = new ErrorHandlerMock();
            var argsSource = new StringSource(args);
            var appSource = new StringSource(app);
            var embededFunctions = new List<IFunction>() { new PrintFunction() };
            Variant result = new(-1);

            try
            {
                var parsedArgs = new Parser(new Lexer(argsSource)).ParseArgs();
                var parsedProgram = new Parser(new Lexer(appSource)).ParseApp();
                var interpreter = new Interpreter(parsedArgs, parsedProgram, errorHandler, printer, embededFunctions);
                result = interpreter.Run();
            }
            catch (ReadableException e)
            {
                errorHandler.HandleException(e);
            }
            return (result, errorHandler.exceptions, printer.output);
            
        }
    }

    [TestClass]
    public class InterpreterBasicTest
    {

        [TestMethod]
        public void ReturnTest1()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){return 1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void MainArgsTest()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a;}");
            Assert.AreEqual(2, result.Item1.GetInt());
        }

        [TestMethod]
        public void UndeclaredVariable()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){return a;}");
            Assert.AreEqual(typeof(UndeclaredVariableException), result.Item2[0].GetType());
        }
    }

}


