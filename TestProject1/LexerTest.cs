using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TKOM;

namespace TKOMTest
{
    [TestClass]
    public class SourceTest
    {
        private static char GetSingleChar(ISource source, uint index)
        {
            char result = 'a';
            for (int i = 0; i <= index; i++)
                result = source.GetNextChar();
            return result;
        }

        private static char GetSingleCharString(string content, uint index)
        {
            return GetSingleChar(new StringSource(content), index);
        }

        private static char GetSingleCharFile(string path, uint index)
        {
            return GetSingleChar(new FileSource(path), index);
        }

        [TestMethod]
        public void StringSourceTest1()
        {
            Assert.AreEqual(GetSingleCharString("test", 0), 't');
        }

        [TestMethod]
        public void StringSourceTest2()
        {
            Assert.AreEqual(GetSingleCharString("test", 1), 'e');
        }

        [TestMethod]
        public void StringSourceTest3()
        {
            Assert.AreEqual(GetSingleCharString("test", 4), '\x03');
        }

        [TestMethod]
        public void StringSourceTest4()
        {
            Assert.AreEqual(GetSingleCharString("test", 5), '\x03');
        }

        [TestMethod]
        public void FileSourceTest1()
        {
            Assert.AreEqual(GetSingleCharFile("../../../Examples/TestFile1.txt", 0), 'i');
        }

        [TestMethod]
        public void FileSourceTest2()
        {
            Assert.AreEqual(GetSingleCharFile("../../../Examples/TestFile2.txt", 0), '\x03');
        }
    }

    [TestClass]
    public class SimpleLexerTest
    {
        private static Token GetSingleToken(string content)
        {
            ILexer lexer = new Lexer(new StringSource(content));
            return lexer.GetNextToken();
        }

        [TestMethod]
        public void SkipCommentTest()
        {
            Assert.AreEqual(GetSingleToken("#comment\n while ").type, TokenType.T_WHILE);
        }

        [TestMethod]
        public void SkipCommentTest2()
        {
            Assert.AreEqual(GetSingleToken("#").type, TokenType.T_ETX);
        }

        [TestMethod]
        public void SkipWhiteTest()
        {
            Assert.AreEqual(GetSingleToken("              while ").type, TokenType.T_WHILE);
        }

        [TestMethod]
        public void SkipCommentAndWhiteTest()
        {
            Assert.AreEqual(GetSingleToken("#commentsdasdasd\n" +
                                            "#commentsdasdasd\n" +
                                            "          while ").type, TokenType.T_WHILE);
        }

        [TestMethod]
        public void CountLineTest()
        {
            Assert.AreEqual(GetSingleToken("#comment\n" +
                                            "#comment\r" +
                                            "#comment\r\n" +
                                            "#comment\n\r" +
                                            "while ").position.lineNumber, (uint)5);
        }

        [TestMethod]
        public void CountColumnTest1()
        {
            Assert.AreEqual(GetSingleToken(" \"string\" ").position.column, (uint)2);
        }

        [TestMethod]
        public void CountColumnTest2()
        {
            Assert.AreEqual(GetSingleToken(" 133 ").position.column, (uint)2);
        }

        [TestMethod]
        public void StringTest1()
        {
            var token = GetSingleToken("\"string\" ");
            Assert.AreEqual(token.value.GetString(), "string");
            Assert.AreEqual(token.type, TokenType.T_STRING);
        }

        [TestMethod]
        public void StringTest2()
        {
            Assert.AreEqual(GetSingleToken("\"string const nice text\nwr\" ").value.GetString(),
                "string const nice text\nwr");
        }

        [TestMethod]
        public void StringWithEscapingTest()
        {
            Assert.AreEqual(GetSingleToken("\"!\"string\" ").value.GetString(), "\"string");
        }

        [TestMethod]
        [ExpectedException(typeof(ETXInStringException))]
        public void StringETXExceptionTest()
        {
            GetSingleToken("\"string");
        }

        [TestMethod]
        [ExpectedException(typeof(ETXInStringException))]
        public void StringETXAfterEscapingExceptionTest()
        {
            GetSingleToken("\"string!");
        }

        [TestMethod]
        public void WhileTest()
        {
            Assert.AreEqual(GetSingleToken(" while ").type, TokenType.T_WHILE);
        }

        [TestMethod]
        public void IntTypeTest()
        {
            Assert.AreEqual(GetSingleToken(" int ").type, TokenType.T_INT_TYPE);
        }

        [TestMethod]
        public void FloatTypeTest()
        {
            Assert.AreEqual(GetSingleToken(" float ").type, TokenType.T_FLOAT_TYPE);
        }

        [TestMethod]
        public void StringTypeTest()
        {
            Assert.AreEqual(GetSingleToken(" string ").type, TokenType.T_STRING_TYPE);
        }

        [TestMethod]
        public void VoidTypeTest()
        {
            Assert.AreEqual(GetSingleToken(" void ").type, TokenType.T_VOID_TYPE);
        }

        [TestMethod]
        public void VarTest()
        {
            Assert.AreEqual(GetSingleToken(" var ").type, TokenType.T_VAR);
        }

        [TestMethod]
        public void IfTest()
        {
            Assert.AreEqual(GetSingleToken(" if ").type, TokenType.T_IF);
        }

        [TestMethod]
        public void ElseTest()
        {
            Assert.AreEqual(GetSingleToken(" else ").type, TokenType.T_ELSE);
        }

        [TestMethod]
        public void ElseIfTest()
        {
            Assert.AreEqual(GetSingleToken(" elif ").type, TokenType.T_ELIF);
        }

        [TestMethod]
        public void MatchTest()
        {
            Assert.AreEqual(GetSingleToken(" match ").type, TokenType.T_MATCH);
        }

        [TestMethod]
        public void CaseTest()
        {
            Assert.AreEqual(GetSingleToken(" case ").type, TokenType.T_CASE);
        }

        [TestMethod]
        public void ReturnTest()
        {
            Assert.AreEqual(GetSingleToken("return").type, TokenType.T_RETURN);
        }

        [TestMethod]
        public void IsTest()
        {
            Assert.AreEqual(GetSingleToken(" is").type, TokenType.T_IS);
        }

        [TestMethod]
        public void IdentifierTest1()
        {
            Assert.AreEqual(GetSingleToken(" print()").type, TokenType.T_IDENTIFIER);
        }

        [TestMethod]
        public void IdentifierTest2()
        {
            Assert.AreEqual(GetSingleToken(" print()").value.GetString(), "print");
        }

        [TestMethod]
        public void IdentifierTest3()
        {
            Assert.AreEqual(GetSingleToken("temp").type, TokenType.T_IDENTIFIER);
        }

        [TestMethod]
        public void IdentifierTest4()
        {
            Assert.AreEqual(GetSingleToken("temp").value.GetString(), "temp");
        }

        [TestMethod]
        public void MultiplicationTest()
        {
            Assert.AreEqual(GetSingleToken(" * ").type, TokenType.T_MULTIPLICATION);
        }

        [TestMethod]
        public void DivisionTest()
        {
            Assert.AreEqual(GetSingleToken(" /").type, TokenType.T_DIVISION);
        }

        [TestMethod]
        public void ModuloTest()
        {
            Assert.AreEqual(GetSingleToken("%").type, TokenType.T_MODULO);
        }

        [TestMethod]
        public void SemiColonTest()
        {
            Assert.AreEqual(GetSingleToken(";").type, TokenType.T_SEMICOLON);
        }

        [TestMethod]
        public void LParenTest()
        {
            Assert.AreEqual(GetSingleToken("(").type, TokenType.T_LPAREN);
        }

        [TestMethod]
        public void RParenTest()
        {
            Assert.AreEqual(GetSingleToken(")").type, TokenType.T_RPAREN);
        }

        [TestMethod]
        public void LBracketTest()
        {
            Assert.AreEqual(GetSingleToken("{").type, TokenType.T_LBRACKET);
        }

        [TestMethod]
        public void RBracketTest()
        {
            Assert.AreEqual(GetSingleToken("}").type, TokenType.T_RBRACKET);
        }

        [TestMethod]
        public void CommaTest()
        {
            Assert.AreEqual(GetSingleToken(",").type, TokenType.T_COMMA);
        }

        [TestMethod]
        public void ColonTest()
        {
            Assert.AreEqual(GetSingleToken(":").type, TokenType.T_COLON);
        }

        [TestMethod]
        public void AssignTest()
        {
            Assert.AreEqual(GetSingleToken("=").type, TokenType.T_ASSIGN);
        }

        [TestMethod]
        public void MinusTest()
        {
            Assert.AreEqual(GetSingleToken("-").type, TokenType.T_MINUS);
        }

        [TestMethod]
        public void PlusTest()
        {
            Assert.AreEqual(GetSingleToken("+").type, TokenType.T_PLUS);
        }

        [TestMethod]
        public void LowerTest()
        {
            Assert.AreEqual(GetSingleToken("<").type, TokenType.T_LOWER);
        }

        [TestMethod]
        public void GreaterTest()
        {
            Assert.AreEqual(GetSingleToken(" > ").type, TokenType.T_GREATER);
        }

        [TestMethod]
        public void EqualTest()
        {
            Assert.AreEqual(GetSingleToken("==").type, TokenType.T_EQUAL);
        }

        [TestMethod]
        public void NotEqualTest()
        {
            Assert.AreEqual(GetSingleToken("!=").type, TokenType.T_NOT_EQUAL);
        }

        [TestMethod]
        public void LowerOrEqualTest()
        {
            Assert.AreEqual(GetSingleToken("<=").type, TokenType.T_LO_OR_EQ);
        }

        [TestMethod]
        public void GreaterOrEqualTest()
        {
            Assert.AreEqual(GetSingleToken(">=").type, TokenType.T_GR_OR_EQ);
        }

        [TestMethod]
        public void MaxIntTest()
        {
            var token = GetSingleToken("2147483647");
            Assert.AreEqual(token.type, TokenType.T_INT);
            Assert.AreEqual(token.value.GetInt(), 2147483647);
        }

        [TestMethod]
        [ExpectedException(typeof(IntOverflowException))]
        public void OverflowExceptionTest()
        {
            GetSingleToken("21474836498s");
        }

        [TestMethod]
        public void MinUIntTest()
        {
            var token = GetSingleToken("0");
            Assert.AreEqual(token.type, TokenType.T_INT);
            Assert.AreEqual(token.value.GetInt(), 0);
        }

        [TestMethod]
        public void IntTest()
        {
            var token = GetSingleToken("123");
            Assert.AreEqual(token.type, TokenType.T_INT);
            Assert.AreEqual(token.value.GetInt(), 123);
        }

        [TestMethod]
        public void FloatTest1()
        {
            var token = GetSingleToken("123.1");
            Assert.AreEqual(token.type, TokenType.T_FLOAT);
            Assert.AreEqual(token.value.GetFloat(), 123.1, 0.001);
        }

        [TestMethod]
        [ExpectedException(typeof(IntOverflowException))]
        public void OverflowFloatTest()
        {
            GetSingleToken("21474836498.1");
        }

        [TestMethod]
        [ExpectedException(typeof(IntOverflowException))]
        public void OverflowFloatTest2()
        {
            GetSingleToken("1.21474836498");
        }

        [TestMethod]
        [ExpectedException(typeof(UnknownTokenException))]
        public void WrongFloatTest()
        {
            GetSingleToken("1.21474836.1");
        }

        [TestMethod]
        [ExpectedException(typeof(UnknownTokenException))]
        public void UnkownTokenExceptionTest()
        {
            GetSingleToken(" ||");
        }

        [TestMethod]
        public void FloatTest2()
        {
            Assert.AreEqual(GetSingleToken("21004.204").value.GetFloat(), 21004.204, 0.001);
        }


        [TestMethod]
        public void MinUFloatTest()
        {
            var token = GetSingleToken("0.0");
            Assert.AreEqual(token.type, TokenType.T_FLOAT);
            Assert.AreEqual(token.value.GetFloat(), 0.0, 0.001);
        }
    }

    [TestClass]
    public class ComplexLexerTest
    {
        private static List<Token> GetTokenList(string content)
        {
            ILexer lexer = new Lexer(new StringSource(content));
            var tokenList = new List<Token>();
            Token token = lexer.GetNextToken();
            while (token.type != TokenType.T_ETX)
            {
                tokenList.Add(token);
                token = lexer.GetNextToken();
            }
            tokenList.Add(token);
            return tokenList;
        }

        [TestMethod]
        public void IntAssignOperationTest()
        {
            var tokenList = GetTokenList(" x = 12");
            Assert.AreEqual(tokenList[0].type, TokenType.T_IDENTIFIER);
            Assert.AreEqual(tokenList[1].type, TokenType.T_ASSIGN);
            Assert.AreEqual(tokenList[2].type, TokenType.T_INT);
            Assert.AreEqual(tokenList[3].type, TokenType.T_ETX);
        }

        [TestMethod]
        public void IntAssignPlusOperationTest()
        {
            var tokenList = GetTokenList(" x = 13213");
            Assert.AreEqual(tokenList[0].type, TokenType.T_IDENTIFIER);
            Assert.AreEqual(tokenList[1].type, TokenType.T_ASSIGN);
            Assert.AreEqual(tokenList[2].type, TokenType.T_INT);
            Assert.AreEqual(tokenList[3].type, TokenType.T_ETX);
            Assert.AreEqual(tokenList[2].value.GetInt(), 13213);
        }

        [TestMethod]
        public void WhileTest()
        {
            var tokenList = GetTokenList(" while(x > 10)\n{x = 1;\r\nprint(\"test\");\r}");
            Assert.AreEqual(tokenList[0].type, TokenType.T_WHILE);
            Assert.AreEqual(tokenList[1].type, TokenType.T_LPAREN);
            Assert.AreEqual(tokenList[2].type, TokenType.T_IDENTIFIER);
            Assert.AreEqual(tokenList[2].value.GetString(), "x");
            Assert.AreEqual(tokenList[3].type, TokenType.T_GREATER);
            Assert.AreEqual(tokenList[4].value.GetInt(), 10);
            Assert.AreEqual(tokenList[4].type, TokenType.T_INT);
            Assert.AreEqual(tokenList[5].type, TokenType.T_RPAREN);
            Assert.AreEqual(tokenList[4].position.lineNumber, (uint)1);
            Assert.AreEqual(tokenList[6].type, TokenType.T_LBRACKET);
            Assert.AreEqual(tokenList[7].type, TokenType.T_IDENTIFIER);
            Assert.AreEqual(tokenList[7].value.GetString(), "x");
            Assert.AreEqual(tokenList[8].type, TokenType.T_ASSIGN);
            Assert.AreEqual(tokenList[9].type, TokenType.T_INT);
            Assert.AreEqual(tokenList[9].value.GetInt(), 1);
            Assert.AreEqual(tokenList[10].type, TokenType.T_SEMICOLON);
            Assert.AreEqual(tokenList[9].position.lineNumber, (uint)2);
            Assert.AreEqual(tokenList[11].type, TokenType.T_IDENTIFIER);
            Assert.AreEqual(tokenList[11].value.GetString(), "print");
            Assert.AreEqual(tokenList[12].type, TokenType.T_LPAREN);
            Assert.AreEqual(tokenList[13].type, TokenType.T_STRING);
            Assert.AreEqual(tokenList[13].position.column, (uint)8);
            Assert.AreEqual(tokenList[13].value.GetString(), "test");
            Assert.AreEqual(tokenList[14].type, TokenType.T_RPAREN);
            Assert.AreEqual(tokenList[15].type, TokenType.T_SEMICOLON);
            Assert.AreEqual(tokenList[16].type, TokenType.T_RBRACKET);
            Assert.AreEqual(tokenList[15].position.lineNumber, (uint)4);
        }

        [TestMethod]
        public void IfTest()
        {
            var tokenList = GetTokenList("if(n <= 1){ return 22; }\nelif (n == 3) { return 5; }\nelse { return 3; }");
            Assert.AreEqual(tokenList[0].type, TokenType.T_IF);
            Assert.AreEqual(tokenList[1].type, TokenType.T_LPAREN);
            Assert.AreEqual(tokenList[2].type, TokenType.T_IDENTIFIER);
            Assert.AreEqual(tokenList[2].value.GetString(), "n");
            Assert.AreEqual(tokenList[3].type, TokenType.T_LO_OR_EQ);
            Assert.AreEqual(tokenList[4].type, TokenType.T_INT);
            Assert.AreEqual(tokenList[4].value.GetInt(), 1);
            Assert.AreEqual(tokenList[5].type, TokenType.T_RPAREN);
            Assert.AreEqual(tokenList[6].type, TokenType.T_LBRACKET);
            Assert.AreEqual(tokenList[7].type, TokenType.T_RETURN);
            Assert.AreEqual(tokenList[8].type, TokenType.T_INT);
            Assert.AreEqual(tokenList[8].value.GetInt(), 22);
            Assert.AreEqual(tokenList[9].type, TokenType.T_SEMICOLON);
            Assert.AreEqual(tokenList[10].type, TokenType.T_RBRACKET);
            Assert.AreEqual(tokenList[11].type, TokenType.T_ELIF);
            Assert.AreEqual(tokenList[12].type, TokenType.T_LPAREN);
            Assert.AreEqual(tokenList[13].type, TokenType.T_IDENTIFIER);
            Assert.AreEqual(tokenList[14].type, TokenType.T_EQUAL);
            Assert.AreEqual(tokenList[15].type, TokenType.T_INT);
            Assert.AreEqual(tokenList[16].type, TokenType.T_RPAREN);
            Assert.AreEqual(tokenList[17].type, TokenType.T_LBRACKET);
            Assert.AreEqual(tokenList[18].type, TokenType.T_RETURN);
            Assert.AreEqual(tokenList[19].type, TokenType.T_INT);
            Assert.AreEqual(tokenList[19].value.GetInt(), 5);
            Assert.AreEqual(tokenList[20].type, TokenType.T_SEMICOLON);
            Assert.AreEqual(tokenList[21].type, TokenType.T_RBRACKET);
            Assert.AreEqual(tokenList[22].type, TokenType.T_ELSE);
            Assert.AreEqual(tokenList[23].type, TokenType.T_LBRACKET);
            Assert.AreEqual(tokenList[24].type, TokenType.T_RETURN);
            Assert.AreEqual(tokenList[25].type, TokenType.T_INT);
            Assert.AreEqual(tokenList[25].value.GetInt(), 3);
            Assert.AreEqual(tokenList[26].type, TokenType.T_SEMICOLON);
            Assert.AreEqual(tokenList[27].type, TokenType.T_RBRACKET);
        }


        [TestMethod]
        [ExpectedException(typeof(IntOverflowException))]
        public void OverflowIntExceptionTest()
        {
             GetTokenList(" x += 21474836498");
        }

        [TestMethod]
        [ExpectedException(typeof(IntOverflowException))]
        public void OverflowFloatExceptionTest()
        {
            GetTokenList(" x += 21474836498.21474836");
        }

        [TestMethod]
        [ExpectedException(typeof(ETXInStringException))]
        public void ETXExceptionTest()
        {
            GetTokenList(" temp = \"test, etx in string");
        }

        [TestMethod]
        public void StringNotOnlyLettersTest()
        {
            var tokenList = GetTokenList(" temp = \"test?, /etx in !!\"s...!tring\"");
            Assert.AreEqual(tokenList[2].value.GetString(), "test?, /etx in !\"s...!tring");
        }

    }
}
