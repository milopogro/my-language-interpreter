using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using TKOM;

namespace TKOMTest
{
    [TestClass]
    public class InterpreterPrinterTest
    {
        [TestMethod]
        public void PrintIntTest1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){ print(a); return 1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
            Assert.AreEqual("2", result.Item3[0]);
        }

        [TestMethod]
        public void PrintIntTest2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){ print(a); return 1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
            Assert.AreEqual("2,1", result.Item3[0]);
        }

        [TestMethod]
        public void PrintIntTest3()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){ print(a); return 1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
            Assert.AreEqual("abc", result.Item3[0]);
        }

        [TestMethod]
        public void PrintIntTest4()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){ print(a);" +
                "print(a+\"d\");" +
                "print(123);" +
                "print(1.1); return 1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
            Assert.AreEqual("abc", result.Item3[0]);
            Assert.AreEqual("abcd", result.Item3[1]);
            Assert.AreEqual("123", result.Item3[2]);
            Assert.AreEqual("1,1", result.Item3[3]);
        }

    }

}


