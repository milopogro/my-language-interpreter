using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TKOM;

namespace TKOMTest
{

    [TestClass]
    public class InterpreterExpressionTest
    {

        [TestMethod]
        public void AddIntInt()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a+1;}");
            Assert.AreEqual(3, result.Item1.GetInt());
        }

        [TestMethod]
        public void AddIntFloat()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a+1.2;}");
            Assert.AreEqual(3, result.Item1.GetInt());
        }

        [TestMethod]
        public void AddIntString1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a+\"1\";}");
            Assert.AreEqual(3, result.Item1.GetInt());
        }

        [TestMethod]
        public void AddIntString2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a+\"abc\";}");
            Assert.AreEqual(2, result.Item1.GetInt());
        }

        [TestMethod]
        public void AddFloatInt()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a+1;}");
            Assert.AreEqual(3.1, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void AddFloatFloat()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a+1.2;}");
            Assert.AreEqual(3.3, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void AddFloatString1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a+\"1,1\";}");
            Assert.AreEqual(3.2, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void AddFloatString2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a+\"abc\";}");
            Assert.AreEqual(2.1, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void AddStringInt()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "string main(a){return a+1;}");
            Assert.AreEqual("abc1", result.Item1.GetString());
        }

        [TestMethod]
        public void AddStringFloat()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "string main(a){return a+1.2;}");
            Assert.AreEqual("abc1,2", result.Item1.GetString());
        }

        [TestMethod]
        public void AddStringString()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "string main(a){return a+\"abc\";}");
            Assert.AreEqual("abcabc", result.Item1.GetString());
        }

        [TestMethod]
        public void SubIntInt()
        {
            var result = InterpreterTestTools.GetResult("5", "int main(a){return a-2;}");
            Assert.AreEqual(3, result.Item1.GetInt());
        }

        [TestMethod]
        public void SubIntFloat1()
        {
            var result = InterpreterTestTools.GetResult("5", "int main(a){return a-1.2;}");
            Assert.AreEqual(4, result.Item1.GetInt());
        }

        [TestMethod]
        public void SubIntFloat2()
        {
            var result = InterpreterTestTools.GetResult("5", "int main(a){return a-1.999;}");
            Assert.AreEqual(4, result.Item1.GetInt());
        }

        [TestMethod]
        public void SubIntString1()
        {
            var result = InterpreterTestTools.GetResult("5", "int main(a){return a-\"1\";}");
            Assert.AreEqual(4, result.Item1.GetInt());
        }

        [TestMethod]
        public void SubIntString2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a-\"abc\";}");
            Assert.AreEqual(2, result.Item1.GetInt());
        }

        [TestMethod]
        public void SubFloatInt()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a-1;}");
            Assert.AreEqual(1.1, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void SubFloatFloat()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a-1.2;}");
            Assert.AreEqual(0.9, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void SubFloatString1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a-\"1,1\";}");
            Assert.AreEqual(1, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void SubFloatString2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a-\"abc\";}");
            Assert.AreEqual(2.1, result.Item1.GetFloat(), 0.001);
        }


        [TestMethod]
        public void SubStringInt()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "string main(a){return a-1;}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }


        [TestMethod]
        public void SubStringFloat()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "string main(a){return a-1.2;}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }


        [TestMethod]
        public void SubStringString()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "string main(a){return a-\"abc\";}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void MulIntInt()
        {
            var result = InterpreterTestTools.GetResult("5", "int main(a){return a*2;}");
            Assert.AreEqual(10, result.Item1.GetInt());
        }

        [TestMethod]
        public void MulIntFloat1()
        {
            var result = InterpreterTestTools.GetResult("5", "int main(a){return a*3.2;}");
            Assert.AreEqual(15, result.Item1.GetInt());
        }

        [TestMethod]
        public void MulIntFloat2()
        {
            var result = InterpreterTestTools.GetResult("5", "int main(a){return a*2.999;}");
            Assert.AreEqual(10, result.Item1.GetInt());
        }

        [TestMethod]
        public void MulIntString1()
        {
            var result = InterpreterTestTools.GetResult("5", "int main(a){return a*\"2\";}");
            Assert.AreEqual(10, result.Item1.GetInt());
        }

        [TestMethod]
        public void MulIntString2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a*\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void MulFloatInt()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a*2;}");
            Assert.AreEqual(4.2, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void MulFloatFloat()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a*2.5;}");
            Assert.AreEqual(5.25, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void MulFloatString1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a*\"1,1\";}");
            Assert.AreEqual(2.31, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void MulFloatString2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a*\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetFloat(), 0.001);
        }


        [TestMethod]
        public void MulStringInt()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "string main(a){return a*1;}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }


        [TestMethod]
        public void MulStringFloat()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "string main(a){return a*1.2;}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }


        [TestMethod]
        public void MulStringString()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "string main(a){return a*\"abc\";}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void DivIntInt()
        {
            var result = InterpreterTestTools.GetResult("6", "int main(a){return a/2;}");
            Assert.AreEqual(3, result.Item1.GetInt());
        }

        [TestMethod]
        public void DivIntFloat1()
        {
            var result = InterpreterTestTools.GetResult("6", "int main(a){return a/2.2;}");
            Assert.AreEqual(3, result.Item1.GetInt());
        }

        [TestMethod]
        public void DivIntFloat2()
        {
            var result = InterpreterTestTools.GetResult("6", "int main(a){return a/2.999;}");
            Assert.AreEqual(3, result.Item1.GetInt());
        }

        [TestMethod]
        public void DivIntString1()
        {
            var result = InterpreterTestTools.GetResult("6", "int main(a){return a/\"2\";}");
            Assert.AreEqual(3, result.Item1.GetInt());
        }


        [TestMethod]
        public void DivIntString2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a/\"abc\";}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void DivFloatInt()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a/2;}");
            Assert.AreEqual(1.05, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void DivFloatFloat1()
        {
            var result = InterpreterTestTools.GetResult("2.2", "float main(a){return a/1.1;}");
            Assert.AreEqual(2, result.Item1.GetFloat(), 0.001);
        }


        [TestMethod]
        public void DivFloatFloat2()
        {
            var result = InterpreterTestTools.GetResult("2.2", "float main(a){return a/0;}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void DivFloatString1()
        {
            var result = InterpreterTestTools.GetResult("2.2", "float main(a){return a/\"1,1\";}");
            Assert.AreEqual(2, result.Item1.GetFloat(), 0.001);
        }


        [TestMethod]
        public void DivFloatString2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a/\"abc\";}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }


        [TestMethod]
        public void DivStringInt()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "string main(a){return a/1;}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }


        [TestMethod]
        public void DivStringFloat()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "string main(a){return a/1.2;}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }


        [TestMethod]
        public void DivStringString()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "string main(a){return a/\"abc\";}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void ModIntInt()
        {
            var result = InterpreterTestTools.GetResult("5", "int main(a){return a%3;}");
            Assert.AreEqual(2, result.Item1.GetInt());
        }

        [TestMethod]
        public void ModIntFloat1()
        {
            var result = InterpreterTestTools.GetResult("5", "int main(a){return a%3.2;}");
            Assert.AreEqual(2, result.Item1.GetInt());
        }

        [TestMethod]
        public void ModIntFloat2()
        {
            var result = InterpreterTestTools.GetResult("5", "int main(a){return a%2.999;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void ModIntString1()
        {
            var result = InterpreterTestTools.GetResult("5", "int main(a){return a%\"3\";}");
            Assert.AreEqual(2, result.Item1.GetInt());
        }

        [TestMethod]
        public void ModIntString2()
        {
            var result  = InterpreterTestTools.GetResult("2", "int main(a){return a%\"abc\";}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void ModFloatInt1()
        {
            var result = InterpreterTestTools.GetResult("5.1", "float main(a){return a%3;}");
            Assert.AreEqual(2.1, result.Item1.GetFloat(), 0.001);
        }


        [TestMethod]
        public void ModFloatInt2()
        {
            var result = InterpreterTestTools.GetResult("5.1", "int main(a){return a%0;}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void ModFloatFloat()
        {
            var result = InterpreterTestTools.GetResult("5.1", "float main(a){return a%2.2;}");
            Assert.AreEqual(0.7, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void ModFloatString1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "float main(a){return a%\"1,1\";}");
            Assert.AreEqual(1, result.Item1.GetFloat(), 0.001);
        }


        [TestMethod]
        public void ModFloatString2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a%\"abc\";}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }


        [TestMethod]
        public void ModStringInt()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a%1;}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }


        [TestMethod]
        public void ModStringFloat()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a%1.2;}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }


        [TestMethod]
        public void ModStringString()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a%\"abc\";}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void LessIntInt1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessIntInt2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<3;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessIntFloat1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<1.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessIntFloat2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<3.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessIntString1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<\"1\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessIntString2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<\"3\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessIntString3()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessIntString4()
        {
            var result = InterpreterTestTools.GetResult("-2", "int main(a){return a<\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessFloatInt1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessFloatInt2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<5;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessFloatFloat1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<1.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessFloatFloat2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<3.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessFloatString1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<\"1,1\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessFloatString2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<\"3,1\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessFloatString3()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessFloatString4()
        {
            var result = InterpreterTestTools.GetResult("-2.1", "int main(a){return a<\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessStringInt1()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a<1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessStringInt2()
        {
            var result = InterpreterTestTools.GetResult("\"0abc\"", "int main(a){return a<1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessStringFloat1()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a<1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessStringFloat2()
        {
            var result = InterpreterTestTools.GetResult("\"0abc\"", "int main(a){return a<1.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessStringString1()
        {
            var result = InterpreterTestTools.GetResult("\"zbc\"", "int main(a){return a<\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessStringString2()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a<\"dbc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualIntInt1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<=1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualIntInt2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<=3;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualIntInt3()
        {
            var result = InterpreterTestTools.GetResult("3", "int main(a){return a<=3;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualIntFloat1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<=1.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualIntFloat2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<=3.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualIntFloat3()
        {
            var result = InterpreterTestTools.GetResult("3", "int main(a){return a<=3.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualIntString1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<=\"1\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualIntString2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<=\"3\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualIntString3()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<=\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualIntString4()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a<=\"2\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualIntString5()
        {
            var result = InterpreterTestTools.GetResult("0", "int main(a){return a<=\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualIntString6()
        {
            var result = InterpreterTestTools.GetResult("-2", "int main(a){return a<=\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualFloatInt1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<=1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualFloatInt2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<=5;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualFloatInt3()
        {
            var result = InterpreterTestTools.GetResult("2.0", "int main(a){return a<=2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualFloatFloat1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<=1.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualFloatFloat2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<=3.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualFloatFloat3()
        {
            var result = InterpreterTestTools.GetResult("3.2", "int main(a){return a<=3.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualFloatString1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<=\"1,1\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualFloatString2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<=\"3,1\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualFloatString3()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<=\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualFloatString4()
        {
            var result = InterpreterTestTools.GetResult("-2.1", "int main(a){return a<=\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualFloatString5()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a<=\"2.1\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualFloatString6()
        {
            var result = InterpreterTestTools.GetResult("0.0", "int main(a){return a<=\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualStringInt1()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a<=1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualStringInt2()
        {
            var result = InterpreterTestTools.GetResult("\"0abc\"", "int main(a){return a<=1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualStringInt3()
        {
            var result = InterpreterTestTools.GetResult("\"0\"", "int main(a){return a<=0;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualStringFloat1()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a<=1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualStringFloat2()
        {
            var result = InterpreterTestTools.GetResult("\"0abc\"", "int main(a){return a<=1.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualStringFloat3()
        {
            var result = InterpreterTestTools.GetResult("\"1,2\"", "int main(a){return a<=1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualStringString1()
        {
            var result = InterpreterTestTools.GetResult("\"zbc\"", "int main(a){return a<=\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualStringString2()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a<=\"dbc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void LessEqualStringString3()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a<=\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterIntInt1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterIntInt2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>3;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterIntFloat1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterIntFloat2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>3.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterIntString1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>\"1\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterIntString2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>\"3\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterIntString3()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterIntString4()
        {
            var result = InterpreterTestTools.GetResult("-2", "int main(a){return a>\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterFloatInt1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterFloatInt2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>5;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterFloatFloat1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterFloatFloat2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>3.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterFloatString1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>\"1,1\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterFloatString2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>\"3,1\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterFloatString3()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterFloatString4()
        {
            var result = InterpreterTestTools.GetResult("-2.1", "int main(a){return a>\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterStringInt1()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a>1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterStringInt2()
        {
            var result = InterpreterTestTools.GetResult("\"0abc\"", "int main(a){return a>1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterStringFloat1()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a>1.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterStringFloat2()
        {
            var result = InterpreterTestTools.GetResult("\"0abc\"", "int main(a){return a>1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterStringString1()
        {
            var result = InterpreterTestTools.GetResult("\"zbc\"", "int main(a){return a>\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterStringString2()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a>\"dbc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualIntInt1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>=1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualIntInt2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>=3;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualIntInt3()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>=2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualIntFloat1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>=1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualIntFloat2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>=3.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualIntFloat3()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>=2.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualIntString1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>=\"1\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualIntString2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>=\"3\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualIntString3()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>=\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualIntString4()
        {
            var result = InterpreterTestTools.GetResult("-2", "int main(a){return a>=\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualIntString5()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a>=\"2\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualIntString6()
        {
            var result = InterpreterTestTools.GetResult("0", "int main(a){return a>=\"aad\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualFloatInt1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>=1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualFloatInt2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>=5;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualFloatInt3()
        {
            var result = InterpreterTestTools.GetResult("2.0", "int main(a){return a>=2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualFloatFloat1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>=1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualFloatFloat2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>=3.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualFloatFloat3()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>=2.1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualFloatString1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>=\"1,1\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualFloatString2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>=\"3,1\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualFloatString3()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>=\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualFloatString4()
        {
            var result = InterpreterTestTools.GetResult("-2.1", "int main(a){return a>=\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualFloatString5()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a>=\"2,1\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualFloatString6()
        {
            var result = InterpreterTestTools.GetResult("0.0", "int main(a){return a>=\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualStringInt1()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a>=1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualStringInt2()
        {
            var result = InterpreterTestTools.GetResult("\"0abc\"", "int main(a){return a>=1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualStringInt3()
        {
            var result = InterpreterTestTools.GetResult("\"0\"", "int main(a){return a>=0;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualStringFloat1()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a>=1.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualStringFloat2()
        {
            var result = InterpreterTestTools.GetResult("\"0abc\"", "int main(a){return a>=1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualStringFloat3()
        {
            var result = InterpreterTestTools.GetResult("\"0,1\"", "int main(a){return a>=0.1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualStringString1()
        {
            var result = InterpreterTestTools.GetResult("\"zbc\"", "int main(a){return a>=\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualStringString2()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a>=\"dbc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void GreaterEqualStringString3()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a>=\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualIntInt1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a==1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualIntInt2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a==2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualIntFloat1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a==1.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualIntFloat2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a==2.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualIntString1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a==\"1\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualIntString2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a==\"2\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualIntString3()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a==\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualIntString4()
        {
            var result = InterpreterTestTools.GetResult("-2", "int main(a){return a==\"-2\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualFloatInt1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a==1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualFloatInt2()
        {
            var result = InterpreterTestTools.GetResult("2.0", "int main(a){return a==2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualFloatFloat1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a==1.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualFloatFloat2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a==2.1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualFloatString1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a==\"1,1\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualFloatString2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a==\"2,1\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualFloatString3()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a==\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualFloatString4()
        {
            var result = InterpreterTestTools.GetResult("0.0", "int main(a){return a==\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualStringInt1()
        {
            var result = InterpreterTestTools.GetResult("\"1\"", "int main(a){return a==1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualStringInt2()
        {
            var result = InterpreterTestTools.GetResult("\"0abc\"", "int main(a){return a==1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualStringFloat1()
        {
            var result = InterpreterTestTools.GetResult("\"1,2\"", "int main(a){return a==1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualStringFloat2()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a==1.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualStringString1()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a==\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void EqualStringString2()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a==\"dbc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualIntInt1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a!=1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualIntInt2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a!=2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualIntFloat1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a!=1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualIntFloat2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a!=2.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualIntString1()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a!=\"1\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualIntString2()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a!=\"2\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualIntString3()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){return a!=\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualIntString4()
        {
            var result = InterpreterTestTools.GetResult("-2", "int main(a){return a!=\"-2\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualFloatInt1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a!=1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualFloatInt2()
        {
            var result = InterpreterTestTools.GetResult("2.0", "int main(a){return a!=2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualFloatFloat1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a!=1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualFloatFloat2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a!=2.1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualFloatString1()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a!=\"1,1\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualFloatString2()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a!=\"2,1\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualFloatString3()
        {
            var result = InterpreterTestTools.GetResult("2.1", "int main(a){return a!=\"abc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualFloatString4()
        {
            var result = InterpreterTestTools.GetResult("0.0", "int main(a){return a!=\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualStringInt1()
        {
            var result = InterpreterTestTools.GetResult("\"1\"", "int main(a){return a!=1;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualStringInt2()
        {
            var result = InterpreterTestTools.GetResult("\"0abc\"", "int main(a){return a!=1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualStringFloat1()
        {
            var result = InterpreterTestTools.GetResult("\"1,2\"", "int main(a){return a!=1.2;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualStringFloat2()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a!=1.2;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualStringString1()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a!=\"abc\";}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void NotEqualStringString2()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a!=\"dbc\";}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void CheckTypeInt1()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(a){return a is int;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void CheckTypeInt2()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(a){return a is float;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void CheckTypeInt3()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(a){return a is string;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void CheckTypeFloat1()
        {
            var result = InterpreterTestTools.GetResult("1.1", "int main(a){return a is float;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void CheckTypeFloat2()
        {
            var result = InterpreterTestTools.GetResult("1.1", "int main(a){return a is int;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void CheckTypeFloat3()
        {
            var result = InterpreterTestTools.GetResult("1.1", "int main(a){return a is string;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void CheckTypeString1()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a is string;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void CheckTypeString2()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a is int;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void CheckTypeString3()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return a is float;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void CheckNegateInt1()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(a){return -a;}");
            Assert.AreEqual(-1, result.Item1.GetInt());
        }

        [TestMethod]
        public void CheckNegatInt2()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(a){return -(-a);}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void CheckNegatFloat1()
        {
            var result = InterpreterTestTools.GetResult("1.1", "float main(a){return -a;}");
            Assert.AreEqual(-1.1, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void CheckNegatFloat2()
        {
            var result = InterpreterTestTools.GetResult("1.1", "float main(a){return -(-a);}");
            Assert.AreEqual(1.1, result.Item1.GetFloat(), 0.001);
        }


        [TestMethod]
        public void CheckNegateString()
        {
            var result = InterpreterTestTools.GetResult("\"abc\"", "int main(a){return -a;}");
            Assert.AreEqual(typeof(IllegalValueOperationException), result.Item2[0].GetType());
        }

    }

}
