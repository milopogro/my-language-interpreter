using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TKOM;

namespace TKOMTest
{
    [TestClass]
    public class InterpreterScopeTest
    {

        [TestMethod]
        public void BasicTest1()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){if(1){a = 2;} return 1;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void ScopeTest1()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){if(1){a = 2;} return a;}");
            Assert.AreEqual(typeof(UndeclaredVariableException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void ScopeTest2()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){var a = 0; if(1){a = 2;} return a;}");
            Assert.AreEqual(2, result.Item1.GetInt());
        }

        [TestMethod]
        public void ScopeTest3()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){var a = 3; if(1){a = a*2;} return a;}");
            Assert.AreEqual(6, result.Item1.GetInt());
        }

        [TestMethod]
        public void BasicTest2()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){var a = 3; if(1){" +
                "if(1){b = 3;}" +
                "a = a*2;} return a;}");
            Assert.AreEqual(6, result.Item1.GetInt());
        }

        [TestMethod]
        public void ScopeTest4()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){var a = 3; if(1){" +
                "if(1){b = 3;}" +
                "a = a*2+b;} return a;}");
            Assert.AreEqual(typeof(UndeclaredVariableException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void BasicTest3()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){var a = 3; while(a<6){" +
                "if(a==5){b = 3;}" +
                "a = a+1;} return a;}");
            Assert.AreEqual(6, result.Item1.GetInt());
        }

        [TestMethod]
        public void ScopeTest5()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){var a = 3; while(a<6){" +
                "if(a==5){b = 3;}" +
                "a = a+b;} return a;}");
            Assert.AreEqual(typeof(UndeclaredVariableException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void BasicTest4()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){var a = 3; while(a<4){" +
                "b = 3;" +
                "a = a+1;} return a;}");
            Assert.AreEqual(4, result.Item1.GetInt());
        }

        [TestMethod]
        public void ScopeTest6()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){var a = 3; while(a<4){" +
                "b = 3;" +
                "a = a+1;} return b;}");
            Assert.AreEqual(typeof(UndeclaredVariableException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void BasicTest5()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){var a = 3;" +
                "c = 2; while(a<4){" +
                "b = 3;" +
                "a = a+1;} return a;}");
            Assert.AreEqual(4, result.Item1.GetInt());
        }

        [TestMethod]
        public void ScopeTest7()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){var a = 3;" +
                "c = 2; while(a<4){" +
                "c = 3;" +
                "a = a+1;} return a;}");
            Assert.AreEqual(typeof(TryingToEditUnmutableVariableException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void ScopeTest8()
        {
            var result = InterpreterTestTools.GetResult("", "int main(){var a = 3;" +
                "c = 2; while(a<4){" +
                "var c = 3;" +
                "a = a+1;} return a;}");
            Assert.AreEqual(typeof(TryingToAddExistingVariableException), result.Item2[0].GetType());
        }

    }

}


