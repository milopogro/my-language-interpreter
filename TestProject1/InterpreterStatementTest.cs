using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TKOM;

namespace TKOMTest
{
    [TestClass]
    public class InterpreterStatementTest
    {

        [TestMethod]
        public void IfStatementTest1()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(a){if(a){ return a;}" +
                "else{return 2;}}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void IfStatementTest2()
        {
            var result = InterpreterTestTools.GetResult("0", "int main(a){if(a){ return a;}" +
                "else{return 2;}}");
            Assert.AreEqual(2, result.Item1.GetInt());
        }

        [TestMethod]
        public void ElifStatementTest1()
        {
            var result = InterpreterTestTools.GetResult("0", "int main(a){" +
                "if(a==0){ return 1;}" +
                "elif(a==1){return 2;}" +
                "else{return 3;}}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void ElifStatementTest2()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(a){" +
                "if(a==0){ return 1;}" +
                "elif(a==1){return 2;}" +
                "else{return 3;}}");
            Assert.AreEqual(2, result.Item1.GetInt());
        }

        [TestMethod]
        public void ElifStatementTest3()
        {
            var result = InterpreterTestTools.GetResult("3", "int main(a){" +
                "if(a==0){ return 1;}" +
                "elif(a==1){return 2;}" +
                "else{return 3;}}");
            Assert.AreEqual(3, result.Item1.GetInt());
        }

        [TestMethod]
        public void ElifStatementTest4()
        {
            var result = InterpreterTestTools.GetResult("2", "int main(a){" +
                "if(a==0){ return 1;}" +
                "elif(a==1){return 2;}" +
                "elif(a==2){return 4;}" +
                "else{return 3;}}");
            Assert.AreEqual(4, result.Item1.GetInt());
        }

        [TestMethod]
        public void AssignStatementTest1()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(var a){a = a +1; return a;}");
            Assert.AreEqual(2, result.Item1.GetInt());
        }

        [TestMethod]
        public void AssignStatementTest2()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(var a){a = a +1; b = 2*a; return b;}");
            Assert.AreEqual(4, result.Item1.GetInt());
        }

        [TestMethod]
        public void AssignStatementTest3()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(var a){" +
                "if(a){a = a +3;}" +
                "return a;}");
            Assert.AreEqual(4, result.Item1.GetInt());
        }

        [TestMethod]
        public void AssignStatementTest4()
        {
            var result = InterpreterTestTools.GetResult("0", "int main(var a){" +
                "if(a){a = a +3;}" +
                "return a;}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void AssignStatementNotMutableTest1()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(a){a = a +1; return a;}");
            Assert.AreEqual(typeof(TryingToEditUnmutableVariableException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void WhileStatementTest1()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(var a){" +
                "while(a<5){a = a + 1;}" +
                "return a;}");
            Assert.AreEqual(5, result.Item1.GetInt());
        }

        [TestMethod]
        public void WhileStatementTest2()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(var a){" +
                "while(a<0){a = a + 1;}" +
                "return a;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void WhileStatementTest3()
        {
            var result = InterpreterTestTools.GetResult("1", "int main(var a){" +
                "while(a<5){" +
                "if(a%2 == 0){return a;}" +
                "a = a + 1;" +
                "}" +
                "return a;}");
            Assert.AreEqual(2, result.Item1.GetInt());
        }


        [TestMethod]
        public void PatternMatchingTest1()
        {
            var result = InterpreterTestTools.GetResult("1",
                "int main(a){match x=a:" +
                "case(x % 2 == 0){ return 0;}" +
                "case(x is int){ return 1;}" +
                "case(x == \"a\"){ return 2;}" +
                "else{ return 3;}}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }

        [TestMethod]
        public void PatternMatchingTest2()
        {
            var result = InterpreterTestTools.GetResult("2",
                "int main(a){match x=a:" +
                "case(x % 2 == 0){ return 0;}" +
                "case(x is int){ return 1;}" +
                "case(x == \"a\"){ return 2;}" +
                "else{ return 3;}}");
            Assert.AreEqual(0, result.Item1.GetInt());
        }

        [TestMethod]
        public void PatternMatchingTest3()
        {
            var result = InterpreterTestTools.GetResult("3.1",
                "int main(a){match x=a:" +
                "case(x % 2 == 0){ return 0;}" +
                "case(x is int){ return 1;}" +
                "case(x == \"a\"){ return 2;}" +
                "else{ return 3;}}");
            Assert.AreEqual(3, result.Item1.GetInt());
        }


        [TestMethod]
        public void PatternMatchingTest4()
        {
            var result = InterpreterTestTools.GetResult("\"a\"",
                "int main(a){match x=a:" +
                "case(x == \"a\"){ return 2;}" +
                "case(x % 2 == 0){ return 0;}" +
                "case(x is int){ return 1;}" +
                "else{ return 3;}}");
            Assert.AreEqual(2, result.Item1.GetInt());
        }

    }

}


