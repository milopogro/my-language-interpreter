using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TKOM;

namespace TKOMTest
{
    public static class TestTools
    {
        public static App ParseAppString(string code)
        {
            return new Parser(new Lexer(new StringSource(code))).ParseApp();
        }

        public static IList<CallParameter> ParseArgsString(string code)
        {
            return new Parser(new Lexer(new StringSource(code))).ParseArgs();
        }
    }

    [TestClass]
    public class ParserFunDefTests
    {
        [TestMethod]
        public void TestSimpleFunDefReadingType()
        {
            App app = TestTools.ParseAppString("int test(){return 0;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            Assert.AreEqual(AppType.T_INT_TYPE, functionDefinition.FunctionType);
        }

        [TestMethod]
        public void TestSimpleFunDefReadingName()
        {
            App app = TestTools.ParseAppString("int test(){return 0;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            Assert.AreEqual("test", functionDefinition.Name);
        }

        [TestMethod]
        public void TestSimpleFunDefReadingParamsCount()
        {
            App app = TestTools.ParseAppString("int test(){return 0;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            Assert.AreEqual(0, functionDefinition.Parameters.Count);
        }

        [TestMethod]
        public void TestFunDefParamsReading()
        {
            App app = TestTools.ParseAppString("int test(var ab, a, b){return 0;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var parameters = functionDefinition.Parameters;
            Assert.AreEqual(true, parameters[0].mutable);
            Assert.AreEqual("ab", parameters[0].name);
            Assert.AreEqual("a", parameters[1].name);
            Assert.AreEqual("b", parameters[2].name);
        }

        [TestMethod]
        [ExpectedException(typeof(NoFunctionException))]
        public void TestNoFunctionException()
        {
            TestTools.ParseAppString("");
        }

        [TestMethod]
        public void TestMoreThanOneSimpleFunDefReading()
        {
            App app = TestTools.ParseAppString("int test1(){return 1;} float test2(){return 2.1;}");
            Assert.AreEqual(AppType.T_INT_TYPE, app.FunctionDir["test1"].FunctionType);
            Assert.AreEqual("test1", app.FunctionDir["test1"].Name);
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test1"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            var numericExpression = (NumericExpression)returnStatement.result;
            Assert.AreEqual(1, numericExpression.Value.GetInt());
            Assert.AreEqual(AppType.T_FLOAT_TYPE, app.FunctionDir["test2"].FunctionType);
            Assert.AreEqual("test2", app.FunctionDir["test2"].Name);
            functionDefinition = (FunctionDefinition)app.FunctionDir["test2"];
            returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            numericExpression = (NumericExpression)returnStatement.result;
            Assert.AreEqual(2.1, numericExpression.Value.GetFloat(), 0.0001);
        }

        [TestMethod]
        [ExpectedException(typeof(NoSemiColonException))]
        public void TestNoSemiColonException()
        {
            TestTools.ParseAppString("int test(){return 0}");
        }

        [TestMethod]
        [ExpectedException(typeof(NoParameterException))]
        public void TestNoParamAfterComaException()
        {
            TestTools.ParseAppString("int test(a,){return 0}");
        }

        [TestMethod]
        [ExpectedException(typeof(NoParameterException))]
        public void TestNoParamAfterVarException()
        {
            TestTools.ParseAppString("int test(var ){return 0;}");
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestNoIdentifierInFunDefException()
        {
            TestTools.ParseAppString("int (){return 0;}");
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestNoLParenInFunDefException()
        {
            TestTools.ParseAppString("int test){return 0;}");
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestNoRParenInFunDefException()
        {
            TestTools.ParseAppString("int test({return 0;}");
        }

        [TestMethod]
        [ExpectedException(typeof(NoBlockException))]
        public void TestNoBlockInFunDefException()
        {
            TestTools.ParseAppString("int test()");
        }

    }

    [TestClass]
    public class ParserStatementTests
    {

        [TestMethod]
        public void TestReturnStatement()
        {
            App app = TestTools.ParseAppString("int test(){return 0;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            Assert.AreEqual(typeof(ReturnStatement), functionDefinition.StatementList[0].GetType());
        }

        [TestMethod]
        public void TestReturnStatementValue()
        {
            App app = TestTools.ParseAppString("int test(){return 0;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            var numericExpression = (NumericExpression)returnStatement.result;
            Assert.AreEqual(0, numericExpression.Value.GetInt());
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectedExpressionException))]
        public void TestNoExceptionAfterReturn()
        {
            TestTools.ParseAppString("int test(){return}");
        }

        [TestMethod]
        public void TestIfStatement()
        {
            App app = TestTools.ParseAppString("int test(a){if(a>0){return 0;} else{return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            Assert.AreEqual(typeof(IfStatement), functionDefinition.StatementList[0].GetType());
        }

        [TestMethod]
        public void TestElifStatementCount0()
        {
            App app = TestTools.ParseAppString("int test(a){if(a>0){return 0;} else{return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(0, ifStatement.elifList.Count);
        }

        [TestMethod]
        public void TestElifStatementCount1()
        {
            App app = TestTools.ParseAppString("int test(a){if(a>0){return 0;} elif(a>-1){return 1;} else{return 2;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(1, ifStatement.elifList.Count);
        }

        [TestMethod]
        public void TestElifStatementCount2()
        {
            App app = TestTools.ParseAppString("int test(a){if(a>0){return 0;} elif(a>-1){return 1;} elif(a>-2){return 2;} else{return 3;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(2, ifStatement.elifList.Count);
        }

        [TestMethod]
        public void TestElseStatementBlock()
        {
            App app = TestTools.ParseAppString("int test(a){if(a>0){return 0;} else{return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(ReturnStatement), ifStatement.elseStatement.block[0].GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(NoBlockException))]
        public void TestElseStatementNoBlockException()
        {
            TestTools.ParseAppString("int test(a){if(a>0){return 0;} else}");
        }

        [TestMethod]
        public void TestElifStatementBlock()
        {
            App app = TestTools.ParseAppString("int test(a){if(a>0){return 0;} elif(a>-1){return 1;} elif(a>-2){return 2;} else{return 3;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(ReturnStatement), ifStatement.elifList[1].block[0].GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(NoConditionException))]
        public void TestNoConditionException1()
        {
            TestTools.ParseAppString("int test(a){if(){return 0;} elif(a>-1){return 1;} elif(a>-2){return 2;} else{return 3;}}");
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestNoLParenException()
        {
            TestTools.ParseAppString("int test(a){if a > 0){return 0;} elif(a>-1){return 1;} elif(a>-2){return 2;} else{return 3;}}");
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestNoRParenException()
        {
            TestTools.ParseAppString("int test(a){if(a > 0{return 0;} elif(a>-1){return 1;} elif(a>-2){return 2;} else{return 3;}}");
        }

        [TestMethod]
        [ExpectedException(typeof(NoBlockException))]
        public void TestNoBlockException()
        {
            TestTools.ParseAppString("int test(a){if(a>0) elif(a>-1){return 1;} elif(a>-2){return 2;} else{return 3;}}");
        }

        [TestMethod]
        [ExpectedException(typeof(NoBlockException))]
        public void TestNoBlockExceptionInElse()
        {
            TestTools.ParseAppString("int test(a){if(a>0) {return 0;} elif(a>-1)elif(a>-2){return 2;} else}");
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestNoRBracketException()
        {
            TestTools.ParseAppString("int test(a){if(a>0) {return 0; elif(a>-1){return 1;} elif(a>-2){return 2;} else{return 3;}}");
        }

        [TestMethod]
        public void TestIfStatementCompareCond()
        {
            App app = TestTools.ParseAppString("int test(a){if(a>0){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(CompareCondition), ifStatement.condition.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectedExpressionException))]
        public void TestCompareCondNoExpressionException()
        {
            TestTools.ParseAppString("int test(a){if(a>){return 0;} else {return 1;}}");
        }

        [TestMethod]
        public void TestCompareCondGreater()
        {
            App app = TestTools.ParseAppString("int test(a){if(a>0){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var compareCondition = (CompareCondition)ifStatement.condition;
            Assert.AreEqual(AppType.T_GREATER, compareCondition.CompareOperation);
        }

        [TestMethod]
        public void TestCompareCondLower()
        {
            App app = TestTools.ParseAppString("int test(a){if(a<0){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var compareCondition = (CompareCondition)ifStatement.condition;
            Assert.AreEqual(AppType.T_LOWER, compareCondition.CompareOperation);
        }

        [TestMethod]
        public void TestCompareCondEqualOrGreater()
        {
            App app = TestTools.ParseAppString("int test(a){if(a>=0){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var compareCondition = (CompareCondition)ifStatement.condition;
            Assert.AreEqual(AppType.T_GR_OR_EQ, compareCondition.CompareOperation);
        }

        [TestMethod]
        public void TestCompareCondEqualOrLower()
        {
            App app = TestTools.ParseAppString("int test(a){if(a<=0){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var compareCondition = (CompareCondition)ifStatement.condition;
            Assert.AreEqual(AppType.T_LO_OR_EQ, compareCondition.CompareOperation);
        }

        [TestMethod]
        public void TestCompareCondEqual()
        {
            App app = TestTools.ParseAppString("int test(a){if(a==0){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var compareCondition = (CompareCondition)ifStatement.condition;
            Assert.AreEqual(AppType.T_EQUAL, compareCondition.CompareOperation);
        }

        [TestMethod]
        public void TestCompareCondNotEqual()
        {
            App app = TestTools.ParseAppString("int test(a){if(a!=0){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var compareCondition = (CompareCondition)ifStatement.condition;
            Assert.AreEqual(AppType.T_NOT_EQUAL, compareCondition.CompareOperation);
        }

        [TestMethod]
        public void TestCompareCondExpressionTypes()
        {
            App app = TestTools.ParseAppString("int test(a){if(a > 0){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var checkTypeCondition = (CompareCondition)ifStatement.condition;
            Assert.AreEqual(typeof(ValueRef), checkTypeCondition.LeftExpression.GetType());
            Assert.AreEqual(typeof(NumericExpression), checkTypeCondition.RightExpression.GetType());
        }

        [TestMethod]
        public void TestCompareCondExpressionValue()
        {
            App app = TestTools.ParseAppString("int test(a){if(a > 0){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var checkTypeCondition = (CompareCondition)ifStatement.condition;
            var leftExpression = (ValueRef)checkTypeCondition.LeftExpression;
            var rightExpression = (NumericExpression)checkTypeCondition.RightExpression;
            Assert.AreEqual("a", leftExpression.Name);
            Assert.AreEqual(0, rightExpression.Value.GetInt());
        }

        [TestMethod]
        public void TestIfStatementCheckTypeCond()
        {
            App app = TestTools.ParseAppString("int test(a){if(a is int){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(CheckTypeCondition), ifStatement.condition.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(NoValueTypeException))]
        public void TestCheckCondNoValueTypeException()
        {
            TestTools.ParseAppString("int test(a){if(a is){return 0;} else {return 1;}}");
        }

        [TestMethod]
        public void TestCheckTypeFloat()
        {
            App app = TestTools.ParseAppString("int test(a){if(a is float){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var checkTypeCondition = (CheckTypeCondition)ifStatement.condition;
            Assert.AreEqual(AppType.T_FLOAT_TYPE, checkTypeCondition.ValueType);
        }

        [TestMethod]
        public void TestCheckTypeInt()
        {
            App app = TestTools.ParseAppString("int test(a){if(a is int){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var checkTypeCondition = (CheckTypeCondition)ifStatement.condition;
            Assert.AreEqual(AppType.T_INT_TYPE, checkTypeCondition.ValueType);
        }

        [TestMethod]
        public void TestCheckTypeString()
        {
            App app = TestTools.ParseAppString("int test(a){if(a is string){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var checkTypeCondition = (CheckTypeCondition)ifStatement.condition;
            Assert.AreEqual(AppType.T_STRING_TYPE, checkTypeCondition.ValueType);
        }

        [TestMethod]
        public void TestCheckTypeExpressionType()
        {
            App app = TestTools.ParseAppString("int test(a){if(a is string){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var checkTypeCondition = (CheckTypeCondition)ifStatement.condition;
            Assert.AreEqual(typeof(ValueRef), checkTypeCondition.Expression.GetType());
        }

        [TestMethod]
        public void TestCheckTypeExpressionValue()
        {
            App app = TestTools.ParseAppString("int test(a){if(a is string){return 0;} else {return 1;}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var ifStatement = (IfStatement)functionDefinition.StatementList[0];
            var checkTypeCondition = (CheckTypeCondition)ifStatement.condition;
            var expression = (ValueRef)checkTypeCondition.Expression;
            Assert.AreEqual("a", expression.Name);
        }

        [TestMethod]
        public void TestWhileStatement()
        {
            App app = TestTools.ParseAppString("int test(var a){while(a < 10){a = a + 1;} return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            Assert.AreEqual(typeof(WhileStatement), functionDefinition.StatementList[0].GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestNoIdentifierAfterVarException()
        {
            TestTools.ParseAppString("int test(){var return a;}");
        }

        [TestMethod]
        [ExpectedException(typeof(AloneIdentifierException))]
        public void TestAloneIdentifierException()
        {
            TestTools.ParseAppString("int test(){var a return a;}");
        }


        [TestMethod]
        [ExpectedException(typeof(AloneIdentifierException))]
        public void TestFunAfterVar()
        {
            TestTools.ParseAppString("int test(){var a(); return a;}");
        }

        [TestMethod]
        public void TestAssignStatement()
        {
            App app = TestTools.ParseAppString("int test(){a = 2; return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            Assert.AreEqual(typeof(AssignStatement), functionDefinition.StatementList[0].GetType());
        }

        [TestMethod]
        public void TestAssignStatementMutableTrue()
        {
            App app = TestTools.ParseAppString("int test(){var a = 2; return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var assignStatement = (AssignStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(true, assignStatement.mutable);
        }

        [TestMethod]
        public void TestAssignStatementMutableFalse()
        {
            App app = TestTools.ParseAppString("int test(){a = 2; return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var assignStatement = (AssignStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(false, assignStatement.mutable);
        }

        [TestMethod]
        public void TestAssignStatementName()
        {
            App app = TestTools.ParseAppString("int test(){a = 2; return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var assignStatement = (AssignStatement)functionDefinition.StatementList[0];
            Assert.AreEqual("a", assignStatement.identifier);
        }

        [TestMethod]
        public void TestAssignStatementRightExpression()
        {
            App app = TestTools.ParseAppString("int test(){a = 2; return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var assignStatement = (AssignStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(NumericExpression), assignStatement.rightValue.GetType());
        }

        [TestMethod]
        public void TestAssignStatementAssignTypeNormal()
        {
            App app = TestTools.ParseAppString("int test(){a = 2; return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var assignStatement = (AssignStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(AppType.T_ASSIGN, assignStatement.assignType);
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectedExpressionException))]
        public void TestNoExpressionInAssignStatement()
        {
            TestTools.ParseAppString("int test(){var a = ; return a;}");
        }

        [TestMethod]
        public void TestFunCallStatement()
        {
            App app = TestTools.ParseAppString("int test(){fun(); return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            Assert.AreEqual(typeof(FunCall), functionDefinition.StatementList[0].GetType());
        }

        [TestMethod]
        public void TestFunCallStatementName()
        {
            App app = TestTools.ParseAppString("int test(){fun(); return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var funCallStatement = (FunCall)functionDefinition.StatementList[0];
            Assert.AreEqual("fun", funCallStatement.FunName);
        }

        [TestMethod]
        public void TestFunCallStatementParams0()
        {
            App app = TestTools.ParseAppString("int test(){fun(); return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var funCallStatement = (FunCall)functionDefinition.StatementList[0];
            Assert.AreEqual(0, funCallStatement.ParameterList.Count);
        }

        [TestMethod]
        public void TestFunCallStatementParams1()
        {
            App app = TestTools.ParseAppString("int test(a, b){fun(a); return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var funCallStatement = (FunCall)functionDefinition.StatementList[0];
            Assert.AreEqual(1, funCallStatement.ParameterList.Count);
        }

        [TestMethod]
        public void TestFunCallStatementParams2()
        {
            App app = TestTools.ParseAppString("int test(a, b){fun(a, b); return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var funCallStatement = (FunCall)functionDefinition.StatementList[0];
            Assert.AreEqual(2, funCallStatement.ParameterList.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestVarInCallParameters()
        {
            TestTools.ParseAppString("int test(a){fun(var a);; return a;}");
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestNoRParenInFunCall()
        {
            TestTools.ParseAppString("int test(a){fun(a; return a;}");
        }

        [TestMethod]
        [ExpectedException(typeof(NoParameterException))]
        public void TestNoParamAfterComaInFunCall()
        {
            TestTools.ParseAppString("int test(a){fun(a,); return a;}");
        }

        [TestMethod]
        public void TestPatternMatchingStatement()
        {
            App app = TestTools.ParseAppString("int test(){match x=funcall():" +
                "case(x % 2 == 0){ print(\"Even number\");}" +
                "case(x is int){ print(\"This is int, but not even\");}" +
                "case(x == \"a\"){ print(\"This is letter !\"a!\"\");}" +
                "else{ print(\"wrrr\");}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            Assert.AreEqual(typeof(PatternMatchingStatement), functionDefinition.StatementList[0].GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestNoIdentifierInPatternMatchingStatement()
        {
            TestTools.ParseAppString("int test(){match =funcall():" +
                "case(x % 2 == 0){ print(\"Even number\");}" +
                "case(x is int){ print(\"This is int, but not even\");}" +
                "case(x == \"a\"){ print(\"This is letter !\"a!\"\");}" +
                "else{ print(\"wrrr\");}}");
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestNoAssignPatternMatchingStatement()
        {
            TestTools.ParseAppString("int test(){match x:" +
                "case(x % 2 == 0){ print(\"Even number\");}" +
                "case(x is int){ print(\"This is int, but not even\");}" +
                "case(x == \"a\"){ print(\"This is letter !\"a!\"\");}" +
                "else{ print(\"wrrr\");}}");
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectedExpressionException))]
        public void TestNoExpressionPatternMatchingStatement()
        {
            TestTools.ParseAppString("int test(){match x=:" +
                "case(x % 2 == 0){ print(\"Even number\");}" +
                "case(x is int){ print(\"This is int, but not even\");}" +
                "case(x == \"a\"){ print(\"This is letter !\"a!\"\");}" +
                "else{ print(\"wrrr\");}}");
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestNoColonPatternMatchingStatement()
        {
            TestTools.ParseAppString("int test(){match x=funCall()" +
                "case(x % 2 == 0){ print(\"Even number\");}" +
                "case(x is int){ print(\"This is int, but not even\");}" +
                "case(x == \"a\"){ print(\"This is letter !\"a!\"\");}" +
                "else{ print(\"wrrr\");}}");
        }

        [TestMethod]
        [ExpectedException(typeof(NoCaseStatementException))]
        public void TestNoCasePatternMatchingStatement()
        {
            TestTools.ParseAppString("int test(){match x=funCall():" +
                "else{ print(\"wrrr\");}}");
        }

        [TestMethod]
        public void TestPatternMatchingStatementCaseCount1()
        {
            App app = TestTools.ParseAppString("int test(){match x=funcall():" +
                "case(x % 2 == 0){ print(\"Even number\");}" +
                "else{ print(\"wrrr\");}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var patternMatchinStatement = (PatternMatchingStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(1, patternMatchinStatement.caseList.Count);
        }

        [TestMethod]
        public void TestPatternMatchingStatementCaseCount2()
        {
            App app = TestTools.ParseAppString("int test(){match x=funcall():" +
                "case(x % 2 == 0){ print(\"Even number\");}" +
                "case(x is int){ print(\"This is int, but not even\");}" +
                "else{ print(\"wrrr\");}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var patternMatchinStatement = (PatternMatchingStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(2, patternMatchinStatement.caseList.Count);
        }

        [TestMethod]
        public void TestPatternMatchingStatementCaseCount3()
        {
            App app = TestTools.ParseAppString("int test(){match x=funcall():" +
                "case(x % 2 == 0){ print(\"Even number\");}" +
                "case(x is int){ print(\"This is int, but not even\");}" +
                "case(x == \"a\"){ print(\"This is letter !\"a!\"\");}" +
                "else{ print(\"wrrr\");}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var patternMatchinStatement = (PatternMatchingStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(3, patternMatchinStatement.caseList.Count);
        }

        [TestMethod]
        public void TestPatternMatchingStatementElseNull()
        {
            App app = TestTools.ParseAppString("int test(){match x=funcall():" +
                "case(x % 2 == 0){ print(\"Even number\");}" +
                "case(x is int){ print(\"This is int, but not even\");}" +
                "case(x == \"a\"){ print(\"This is letter !\"a!\"\");}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var patternMatchinStatement = (PatternMatchingStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(null, patternMatchinStatement.elseStatement);
        }

        [TestMethod]
        public void TestPatternMatchingStatementElseNotNull()
        {
            App app = TestTools.ParseAppString("int test(){match x=funcall():" +
                "case(x % 2 == 0){ print(\"Even number\");}" +
                "case(x is int){ print(\"This is int, but not even\");}" +
                "case(x == \"a\"){ print(\"This is letter !\"a!\"\");}" +
                "else{ print(\"wrrr\");}}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var patternMatchinStatement = (PatternMatchingStatement)functionDefinition.StatementList[0];
            Assert.AreNotEqual(null, patternMatchinStatement.elseStatement);
        }

    }

    [TestClass]
    public class ParserExpressionTests
    {
        [TestMethod]
        public void TestAddExpression()
        {
            App app = TestTools.ParseAppString("int test(a){return a+1;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(AddOperationExpression), returnStatement.result.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectedExpressionException))]
        public void TestNoRightExpressionAddException()
        {
            TestTools.ParseAppString("int test(a){return a+;}");
        }

        [TestMethod]
        public void TestSubExpression()
        {
            App app = TestTools.ParseAppString("int test(a){return a-1;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(SubOperationExpression), returnStatement.result.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectedExpressionException))]
        public void TestNoRightExpressionSubException()
        {
            TestTools.ParseAppString("int test(a){return a-;}");
        }

        [TestMethod]
        public void TestModExpression()
        {
            App app = TestTools.ParseAppString("int test(a){return a%2;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(ModOperationExpression), returnStatement.result.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectedExpressionException))]
        public void TestNoRightExpressionModException()
        {
            TestTools.ParseAppString("int test(a){return a%;}");
        }

        [TestMethod]
        public void TestMulExpression()
        {
            App app = TestTools.ParseAppString("int test(a){return a*2;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(MulOperationExpression), returnStatement.result.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectedExpressionException))]
        public void TestNoRightExpressionMulException()
        {
            TestTools.ParseAppString("int test(a){return a*;}");
        }

        [TestMethod]
        public void TestDivExpression()
        {
            App app = TestTools.ParseAppString("int test(a){return a/2;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(DivOperationExpression), returnStatement.result.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectedExpressionException))]
        public void TestNoRightExpressionDivException()
        {
            TestTools.ParseAppString("int test(a){return a/;}");
        }

        [TestMethod]
        public void TestNegateExpression()
        {
            App app = TestTools.ParseAppString("int test(a){return -a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(NegateExpression), returnStatement.result.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(AloneNegateException))]
        public void TestAloneNegateException()
        {
            TestTools.ParseAppString("int test(a){return -;}");
        }

        [TestMethod]
        public void TestNumericExpressionInt()
        {
            App app = TestTools.ParseAppString("int test(a){return 1;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(NumericExpression), returnStatement.result.GetType());
        }

        [TestMethod]
        public void TestNumericExpressionFloat()
        {
            App app = TestTools.ParseAppString("int test(a){return 1.1;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(NumericExpression), returnStatement.result.GetType());
        }

        [TestMethod]
        public void TestStringExpression()
        {
            App app = TestTools.ParseAppString("int test(a){return \"wrrr\";}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(StringExpression), returnStatement.result.GetType());
        }

        [TestMethod]
        public void TestStringAddExpression()
        {
            App app = TestTools.ParseAppString("int test(a){return \"wrrr\" + \"wrrr\";}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(AddOperationExpression), returnStatement.result.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(IllegalOperationException))]
        public void TestStringSubExpression1()
        {
            TestTools.ParseAppString("int test(a){return \"wrrr\" - \"wrrr\";}");
        }

        [TestMethod]
        [ExpectedException(typeof(IllegalOperationException))]
        public void TestStringSubExpression2()
        {
            TestTools.ParseAppString("int test(a){return \"wrrr\" - 1;}");
        }

        [TestMethod]
        [ExpectedException(typeof(IllegalOperationException))]
        public void TestStringSubExpression3()
        {
            TestTools.ParseAppString("int test(a){return (\"wrrr\") - 1;}");
        }

        [TestMethod]
        public void TestValueRefExpression()
        {
            App app = TestTools.ParseAppString("int test(a){return a;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(ValueRef), returnStatement.result.GetType());
        }

        [TestMethod]
        public void TestFunCallExpression()
        {
            App app = TestTools.ParseAppString("int test(a){return funCall();}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(FunCall), returnStatement.result.GetType());
        }

        [TestMethod]
        public void TestBracketExpression()
        {
            App app = TestTools.ParseAppString("int test(a){return (a+1);}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(BracketExpression), returnStatement.result.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(ExpectedExpressionException))]
        public void TestEmptyBracketExpressionException()
        {
            TestTools.ParseAppString("int test(a){return ();}");
        }

        [TestMethod]
        [ExpectedException(typeof(UnexpectedTokenException))]
        public void TestNoRParenInBracketExpressionException()
        {
            TestTools.ParseAppString("int test(a){return (a+1;}");
        }
    }

    [TestClass]
    public class ParserComplexExpressionTests
    {
        [TestMethod]
        public void TestComplexExpression1()
        {
            App app = TestTools.ParseAppString("int test(a){return 2*(a+1);}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(MulOperationExpression), returnStatement.result.GetType());
            var mulOperation = (MulOperationExpression)returnStatement.result;
            Assert.AreEqual(typeof(BracketExpression), mulOperation.RightExpression.GetType());
            Assert.AreEqual(typeof(NumericExpression), mulOperation.LeftExpression.GetType());
            var bracketExpression = (BracketExpression)mulOperation.RightExpression;
            Assert.AreEqual(typeof(AddOperationExpression), bracketExpression.Expression.GetType());
            var addOperation = (AddOperationExpression)bracketExpression.Expression;
            Assert.AreEqual(typeof(ValueRef), addOperation.LeftExpression.GetType());
            Assert.AreEqual(typeof(NumericExpression), addOperation.RightExpression.GetType());
        }

        [TestMethod]
        public void TestComplexExpression2()
        {
            App app = TestTools.ParseAppString("int test(a){return (3/funCall())/(a%5);}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(DivOperationExpression), returnStatement.result.GetType());
            var divOperation = (DivOperationExpression)returnStatement.result;
            Assert.AreEqual(typeof(BracketExpression), divOperation.RightExpression.GetType());
            Assert.AreEqual(typeof(BracketExpression), divOperation.LeftExpression.GetType());
            var bracketExpressionR = (BracketExpression)divOperation.RightExpression;
            var bracketExpressionL = (BracketExpression)divOperation.LeftExpression;
            Assert.AreEqual(typeof(DivOperationExpression), bracketExpressionL.Expression.GetType());
            Assert.AreEqual(typeof(ModOperationExpression), bracketExpressionR.Expression.GetType());
            var divOperation2 = (DivOperationExpression)bracketExpressionL.Expression;
            var modOperation = (ModOperationExpression)bracketExpressionR.Expression;
            Assert.AreEqual(typeof(NumericExpression), divOperation2.LeftExpression.GetType());
            Assert.AreEqual(typeof(FunCall), divOperation2.RightExpression.GetType());
            Assert.AreEqual(typeof(ValueRef), modOperation.LeftExpression.GetType());
            Assert.AreEqual(typeof(NumericExpression), modOperation.RightExpression.GetType());
        }

        [TestMethod]
        public void TestComplexExpression3()
        {
            App app = TestTools.ParseAppString("int test(a){return 2*3*a+2;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(AddOperationExpression), returnStatement.result.GetType());
            var addOperation = (AddOperationExpression)returnStatement.result;
            Assert.AreEqual(typeof(NumericExpression), addOperation.RightExpression.GetType());
            Assert.AreEqual(typeof(MulOperationExpression), addOperation.LeftExpression.GetType());
            var mulOperation = (MulOperationExpression)addOperation.LeftExpression;
            Assert.AreEqual(typeof(MulOperationExpression), mulOperation.LeftExpression.GetType());
            Assert.AreEqual(typeof(ValueRef), mulOperation.RightExpression.GetType());
            var mulOperation2 = (MulOperationExpression)mulOperation.LeftExpression;
            Assert.AreEqual(typeof(NumericExpression), mulOperation2.LeftExpression.GetType());
            Assert.AreEqual(typeof(NumericExpression), mulOperation2.RightExpression.GetType());
        }

        [TestMethod]
        public void TestComplexExpression4()
        {
            App app = TestTools.ParseAppString("int test(a){return -3*-a-2;}");
            var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
            var returnStatement = (ReturnStatement)functionDefinition.StatementList[0];
            Assert.AreEqual(typeof(SubOperationExpression), returnStatement.result.GetType());
            var addOperation = (SubOperationExpression)returnStatement.result;
            Assert.AreEqual(typeof(NumericExpression), addOperation.RightExpression.GetType());
            Assert.AreEqual(typeof(MulOperationExpression), addOperation.LeftExpression.GetType());
            var mulOperation = (MulOperationExpression)addOperation.LeftExpression;
            Assert.AreEqual(typeof(NegateExpression), mulOperation.LeftExpression.GetType());
            Assert.AreEqual(typeof(NegateExpression), mulOperation.RightExpression.GetType());
            var negateExpressionL = (NegateExpression)mulOperation.LeftExpression;
            var negateExpressionR = (NegateExpression)mulOperation.RightExpression;
            Assert.AreEqual(typeof(NumericExpression), negateExpressionL.Expression.GetType());
            Assert.AreEqual(typeof(ValueRef), negateExpressionR.Expression.GetType());
        }

        [TestMethod]
        [ExpectedException(typeof(AloneNegateException))]
        public void TestComplexExpression5()
        {
            TestTools.ParseAppString("int test(a){return -3*--a-2;}");
        }

        [TestClass]
        public class ParserComplexStatementTests
        {
            [TestMethod]
            public void TestComplexStatement1()
            {
                App app = TestTools.ParseAppString("string test(n, toCopy){ " +
                    "var result = \"\";" +
                    "var i = 0;" +
                    "while (i < n){" +
                    "result = result + toCopy;" +
                    "i = i + 1;}" +
                    "return result;}" +
                    "int main(){" +
                    "test(2, \"test\");" +
                    "return 0;}");

                Assert.AreEqual(2, app.FunctionDir.Count);
                var functionDefinition = (FunctionDefinition)app.FunctionDir["test"];
                Assert.AreEqual(4, functionDefinition.StatementList.Count);
                var mainDefinition = (FunctionDefinition)app.FunctionDir["main"];
                Assert.AreEqual(2, mainDefinition.StatementList.Count);
                Assert.AreEqual(typeof(FunCall), mainDefinition.StatementList[0].GetType());
                Assert.AreEqual(typeof(ReturnStatement), mainDefinition.StatementList[1].GetType());
                Assert.AreEqual(typeof(AssignStatement), functionDefinition.StatementList[0].GetType());
                Assert.AreEqual(typeof(AssignStatement), functionDefinition.StatementList[1].GetType());
                Assert.AreEqual(typeof(WhileStatement), functionDefinition.StatementList[2].GetType());
                Assert.AreEqual(typeof(ReturnStatement), functionDefinition.StatementList[3].GetType());
            }

            [TestMethod]
            public void TestComplexStatement2()
            {
                App app = TestTools.ParseAppString("" +
                    "int fibonacci(n){ " +
                    "   if(n < 1){ return 0;} " +
                    "   if(n < 2){ return 1;} " +
                    "   return Fibonacci(n-1) + Fibonacci(n-2); }" +
                    "" +
                    "int mathOperation(a, b){ " +
                    "   return (a + b) * a / b; }" +
                    "" +
                    "string copyString(n, toCopy) {" +
                    "   var result = \" \"; " +
                    "   var i = 0; " +
                    "   while (i < n) { " +
                    "       result = result + toCopy; " +
                    "       i = i + 1; }" +
                    "   return result; }" +
                    "" +
                    "int main() { " +
                    "   a = fibonacci(5); " +
                    "   b = \"Result of Fibonacci for five:\" + a; " +
                    "   print(b); " +
                    "   var c = 1; " +
                    "   c = mathOperation(fibonacci(4), c); " +
                    "   print(copyString(7 / 2, \"test\")); " +
                    "   return 0; }");

                Assert.AreEqual(4, app.FunctionDir.Count);
                var functionDefinition = (FunctionDefinition)app.FunctionDir["fibonacci"];
                Assert.AreEqual(3, functionDefinition.StatementList.Count);
                functionDefinition = (FunctionDefinition)app.FunctionDir["mathOperation"];
                Assert.AreEqual(1, functionDefinition.StatementList.Count);
                functionDefinition = (FunctionDefinition)app.FunctionDir["copyString"];
                Assert.AreEqual(4, functionDefinition.StatementList.Count);
                functionDefinition = (FunctionDefinition)app.FunctionDir["main"];
                Assert.AreEqual(7, functionDefinition.StatementList.Count);
            }

            [TestMethod]
            public void TestComplexStatement3()
            {
                App app = TestTools.ParseAppString("" +
                    "int main(salary, employmentType, tax, insurance){ " +
                    "   var result = 0; " +
                    "   if(employmentType == 0) {result = salary;} " +
                    "   elif(employmentType == 1) {result = salary - tax*salary;} " +
                    "   elif(employmentType == 2) {result = salary - tax*salary - insurance*tax;} " +
                    "   else {print(\"Unknown employment type\");}" +
                    " " +
                    "   match x = result: " +
                    "       case(x == 2061){ print(\"You will earn lowest net salary\");} " +
                    "       case(result is int){ print(\"You will earn\" + result + \"zl\");} " +
                    "       else {  fractionalPart = result - toInt(fractional); " +
                    "               print(\"You will earn\" + result-fractionalPart + \"zl\" + \"and\" + fractionalPart + \"gr\"); " +
                    "       } " +
                    "   return 0; }; ");

                Assert.AreEqual(1, app.FunctionDir.Count);
                var functionDefinition = (FunctionDefinition)app.FunctionDir["main"];
                Assert.AreEqual(4, functionDefinition.StatementList.Count);
            }
        }

    }


    [TestClass]
    public class ArgsParserTests
    {
        [TestMethod]
        public void TestArgs1()
        {
            var args = TestTools.ParseArgsString("1, 2.1, \"abc\"");
            var arg1 = (NumericExpression)args[0].expression;
            Assert.AreEqual(1, arg1.Value.GetInt());
            var arg2 = (NumericExpression)args[1].expression;
            Assert.AreEqual(2.1, arg2.Value.GetFloat(), 0.001);
            var arg3 = (StringExpression)args[2].expression;
            Assert.AreEqual("abc", arg3.Value);
        }

        [TestMethod]
        public void TestArgs2()
        {
            var args = TestTools.ParseArgsString("");
            Assert.AreEqual(0, args.Count);
        }

        [TestMethod]
        public void TestArgs3()
        {
            var args = TestTools.ParseArgsString("2.1");
            var arg1 = (NumericExpression)args[0].expression;
            Assert.AreEqual(2.1, arg1.Value.GetFloat(), 0.001);
        }
    }
}
