using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using TKOM;

namespace TKOMTest
{
    [TestClass]
    public class InterpreterFunctionCallTest
    {
        [TestMethod]
        public void WrongNumberOfArgsTest1()
        {
            var result = InterpreterTestTools.GetResult("2, 5, 3, 1", "int main(a, b, c){return a*b%c;}");
            Assert.AreEqual(typeof(WrongNumberOfParametersException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void WrongNumberOfArgsTest2()
        {
            var result = InterpreterTestTools.GetResult("2, 5, 3", "int main(a, b, c, d){return a*b%c;}");
            Assert.AreEqual(typeof(WrongNumberOfParametersException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void NoMainException()
        {
            var result = InterpreterTestTools.GetResult("12, 2.1, 3", "int test(a, b, c){return -(b*a)+c;}");
            Assert.AreEqual(typeof(NoMainException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void FunctionNotDeclared()
        {
            var result = InterpreterTestTools.GetResult("12, 2.1, 3", "int main(a, b, c){abcfun(); return -(b*a)+c;}");
            Assert.AreEqual(typeof(FunctionNotDeclaredException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void OwnFunctionCall1()
        {
            var result = InterpreterTestTools.GetResult("2, 3", "int fun(c, d){return c*d;} int main(a, b){return fun(a,b);}");
            Assert.AreEqual(6, result.Item1.GetInt());
        }

        [TestMethod]
        public void OwnFunctionCall2()
        {
            var result = InterpreterTestTools.GetResult("2, 3", "int fun(c, d){return c*d;} int main(a, b){var c = fun(a,b); return c+3;}");
            Assert.AreEqual(9, result.Item1.GetInt());
        }

        [TestMethod]
        public void OwnFunctionCallCovertTest1()
        {
            var result = InterpreterTestTools.GetResult("2, 3.2", "int fun(c, d){return c*d;} int main(a, b){var c = fun(a,b); return c+3;}");
            Assert.AreEqual(9, result.Item1.GetInt());
        }

        [TestMethod]
        public void OwnFunctionCallCovertTest2()
        {
            var result = InterpreterTestTools.GetResult("2.1, 3.6", "int fun(c, d){return c*d;} int main(a, b){var c = fun(a,b); return c+3;}");
            Assert.AreEqual(10, result.Item1.GetInt());
        }

        [TestMethod]
        public void OwnFunctionCallRecursiveTest1()
        {
            var result = InterpreterTestTools.GetResult(
                "2.1, 3.6", "int fibonacci(n)" +
                "{ if (n < 1) { return 0; }" +
                "if (n < 2) { return 1; }" +
                "return fibonacci(n - 1) + fibonacci(n - 2);}" +
                "int main(a, b){var c = fibonacci(3); return c+3;}");
            Assert.AreEqual(5, result.Item1.GetInt());
        }

        [TestMethod]
        public void OwnFunctionCallRecursiveTest2()
        {
            var result = InterpreterTestTools.GetResult(
                "2.1, 3.6", "int fibonacci(n)" +
                "{ if (n < 1) { return 0; }" +
                "if (n < 2) { return 1; }" +
                "return fibonacci(n - 1) + fibonacci(n - 2);}" +
                "int main(a, b){var c = fibonacci(30); return c+3;}");
            Assert.AreEqual(832043, result.Item1.GetInt());
        }

        [TestMethod]
        public void OwnFunctionCallRecursiveTest3()
        {
            var result = InterpreterTestTools.GetResult(
                "2.1, 3.6", "int fibonacci(n)" +
                "{ if (n < 1) { return 0; }" +
                "if (n < 2) { return 1; }" +
                "return fibonacci(n - 1) + fibonacci(n - 2);}" +
                "int main(a, b){var c = fibonacci(3)+fibonacci(3); return c+3;}");
            Assert.AreEqual(7, result.Item1.GetInt());
        }

        [TestMethod]
        public void OwnFunctionCallMoreThanOneFunction1()
        {
            var result = InterpreterTestTools.GetResult(
                "2, 3", 
                "int fun(c, d){return c*d;} " +
                "float fun2(c,d){return c+d;}" +
                "float main(a, b){var c = fun2(a,b)*1.11*fun(a,b); return c;}");
            Assert.AreEqual(33.3, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void OwnFunctionCallNoReturnTest1()
        {
            var result = InterpreterTestTools.GetResult(
                "2, 3",
                "int fun(c, d){return c*d;} " +
                "float fun2(c,d){return c+d;}" +
                "float main(a, b){var c = fun2(a,b)*1.11*fun(a,b);}");
            Assert.AreEqual(typeof(NoReturnException), result.Item2[0].GetType());
        }

        [TestMethod]
        public void OwnFunctionCallNoReturnTest2()
        {
            var result = InterpreterTestTools.GetResult(
                "2, 3",
                "int fun(c, d){return c*d;} " +
                "float fun2(c,d){return c+d;}" +
                "void main(a, b){var c = fun2(a,b)*1.11*fun(a,b);}");
            Assert.AreEqual(-1, result.Item1.GetInt());
        }

        [TestMethod]
        public void OwnFunctionCallNoReturnTest3()
        {
            var result = InterpreterTestTools.GetResult(
                "2, 3",
                "void fun(c, d){return c*d;} " +
                "int main(a, b){return fun(a,b);}");
            Assert.AreEqual(-1, result.Item1.GetInt());
        }

    }

}


