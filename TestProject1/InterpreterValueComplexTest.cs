using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TKOM;

namespace TKOMTest
{
    [TestClass]
    public class InterpreterValueComplexTest
    {

        [TestMethod]
        public void ComplexValueTest1()
        {
            var result = InterpreterTestTools.GetResult("12, 2.1, 3", "float main(a, b, c){return -(b*a)+c;}");
            Assert.AreEqual(-22.2, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void ComplexValueTest2()
        {
            var result = InterpreterTestTools.GetResult("1.2, 2.1, \"abc\", 2", "float main(a, b, c, d){return (b*a*d+b)+c;}");
            Assert.AreEqual(7.14, result.Item1.GetFloat(), 0.001);
        }

        [TestMethod]
        public void ComplexValueTest3()
        {
            var result = InterpreterTestTools.GetResult("2, 5, 3", "int main(a, b, c){return a*b%c;}");
            Assert.AreEqual(1, result.Item1.GetInt());
        }
    }

}


