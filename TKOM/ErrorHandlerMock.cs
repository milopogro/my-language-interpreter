﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKOM
{
    public class ErrorHandlerMock : IErrorHandler
    {
        public List<ReadableException> exceptions = new();

        public void HandleException(ReadableException e)
        {
            exceptions.Add(e);
        }
    }
}
