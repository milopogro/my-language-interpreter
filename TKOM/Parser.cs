﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKOM
{
    public class Parser : IParser
    {
        private readonly ILexer lexer;
        private IDictionary<TokenType, AppType> tokenTypeDir;
        private IDictionary<string, IFunction> functionDir;
        private AppType CurrentTokenType { get { return Remap(lexer.Token.type); } }

        public Parser(ILexer lexer)
        {
            this.lexer = lexer;
            lexer.GetNextToken();
            this.functionDir = new Dictionary<string, IFunction>();
            InitTokenTypeDir();
        }

        public App ParseApp()
        {
            var function = TryParseFunDef();

            if (function is null)
                throw new NoFunctionException(lexer.GetPosition());
            while (function is not null)
            {
                functionDir.Add(function.Name, function);
                function = TryParseFunDef();
            }


            return new App(functionDir);
        }

        public IList<CallParameter> ParseArgs()
        {
            return ParseCallParameters();
        }

        private AppType Remap(TokenType tokenType)
        {
            AppType parserTokenType;
            if (tokenTypeDir.TryGetValue(tokenType, out parserTokenType))
                return parserTokenType;
            throw new UnknownTokenTypeException(lexer.GetPreviousPosition());
        }

        bool TokenIs(AppType type)
        {
            if (CurrentTokenType != type)
                return false;
            return true;
        }

        UnexpectedTokenException UnexpectedToken(AppType expectedTokenType)
        {
            return new UnexpectedTokenException(lexer.GetPreviousPosition(), expectedTokenType, CurrentTokenType);
        }

        bool CheckAndConsume(AppType type)
        {
            if (!TokenIs(type))
                return false;
            lexer.GetNextToken();
            return true;
        }

        private void CheckSemiColon()
        {
            var currentPostition = lexer.GetPreviousPosition();
            if (!CheckAndConsume(AppType.T_SEMICOLON))
                throw new NoSemiColonException(currentPostition);
        }

        private void TokenIsOrException(AppType tokenType)
        {
            if (!TokenIs(tokenType))
                throw UnexpectedToken(tokenType);
        }

        private void CheckAndConsumeOrException(AppType tokenType)
        {
            if (!CheckAndConsume(tokenType))
                throw UnexpectedToken(tokenType);
        }

        private FunctionDefinition TryParseFunDef()
        {
            if (!TokenIs(AppType.T_FLOAT_TYPE)&&
                !TokenIs(AppType.T_INT_TYPE)&&
                !TokenIs(AppType.T_STRING_TYPE)&&
                !TokenIs(AppType.T_VOID_TYPE))
                return null;

            var startPostition = lexer.GetPreviousPosition();

            var type = CurrentTokenType;
            lexer.GetNextToken();

            TokenIsOrException(AppType.T_IDENTIFIER);

            var name = lexer.Token.value.GetString();
            lexer.GetNextToken();

            CheckAndConsumeOrException(AppType.T_LPAREN);

            var parameters = ParseDefParameters();

            CheckAndConsumeOrException(AppType.T_RPAREN);

            var block = TryParseBlock();
            if (block is null)
                throw new NoBlockException(lexer.GetPreviousPosition());

            return new FunctionDefinition(name, type, parameters, block, startPostition);
        }

        private IList<DefParameter> ParseDefParameters()
        {
            var parameterList = new List<DefParameter>();
            var parameter = TryParseDefParameter();
            if (parameter is null)
                return parameterList;

            parameterList.Add(parameter);
            while (CheckAndConsume(AppType.T_COMMA))
            {
                parameter = TryParseDefParameter();
                if (parameter is null)
                    throw new NoParameterException(lexer.GetPreviousPosition());
                parameterList.Add(parameter);

            }
            return parameterList;
        }

        private DefParameter TryParseDefParameter()
        {
            bool mutable = CheckAndConsume(AppType.T_VAR);
            if (!TokenIs(AppType.T_IDENTIFIER) && !mutable)
                return null;
            else if (!TokenIs(AppType.T_IDENTIFIER) && mutable)
                throw new NoParameterException(lexer.GetPreviousPosition());
            var parameterName = lexer.Token.value.GetString();
            lexer.GetNextToken();
            return new DefParameter(mutable, parameterName);
        }

        private IList<IStatement> TryParseBlock()
        {
            if (!CheckAndConsume(AppType.T_LBRACKET))
                return null;

            var statementList = new List<IStatement>();
            IStatement statement = TryParseIfStatement() ??
                            TryParseWhileStatement() ??
                            TryParseAssignOrFunCallStatement() ??
                            TryParsePatternMatchingStatement() ??
                            TryParseReturnStatement();

            while (statement is not null)
            {
                statementList.Add(statement);
                statement = TryParseIfStatement() ??
                TryParseWhileStatement() ??
                TryParseAssignOrFunCallStatement() ??
                TryParsePatternMatchingStatement() ?? 
                TryParseReturnStatement();

            }

            CheckAndConsumeOrException(AppType.T_RBRACKET);
            return statementList;
        }

        private IStatement TryParseReturnStatement()
        {
            if (!CheckAndConsume(AppType.T_RETURN))
                return null;

            var startPostition = lexer.GetPreviousPosition();
            var result = TryParseExpression();
            if (result is null)
                throw new ExpectedExpressionException(lexer.GetPreviousPosition());

            CheckSemiColon();

            return new ReturnStatement(result, startPostition);
        }

        private IStatement TryParseIfStatement()
        {
            if (!CheckAndConsume(AppType.T_IF))
                return null;

            var startPostition = lexer.GetPreviousPosition();
            var condAndBlock = ParseCondAndBlock();

            var elifList = new List<ElifStatement>();
            var elifStatement = TryParseElifStatement();

            while (elifStatement is not null)
            {
                elifList.Add(elifStatement);
                elifStatement = TryParseElifStatement();
            }

            var elseStatement = TryParseElseStatement();

            return new IfStatement(condAndBlock.Item1, condAndBlock.Item2, elifList, elseStatement, startPostition);
        }

        private ElifStatement TryParseElifStatement()
        {
            if (!CheckAndConsume(AppType.T_ELIF))
                return null;
            var startPostition = lexer.GetPreviousPosition();
            var condAndBlock = ParseCondAndBlock();

            return new ElifStatement(condAndBlock.Item1, condAndBlock.Item2, startPostition);
        }

        private (IExpression, IList<IStatement>) ParseCondAndBlock()
        {
            CheckAndConsumeOrException(AppType.T_LPAREN);

            var condition = TryParseCondition();
            if (condition is null)
                throw new NoConditionException(lexer.GetPreviousPosition());

            CheckAndConsumeOrException(AppType.T_RPAREN);

            var block = TryParseBlock();
            if (block is null)
                throw new NoBlockException(lexer.GetPreviousPosition());

            return (condition, block);
        }

        private ElseStatement TryParseElseStatement()
        {
            if (!CheckAndConsume(AppType.T_ELSE))
                return null;
            var startPostition = lexer.GetPreviousPosition();
            var block = TryParseBlock();
            if (block is null)
                throw new NoBlockException(lexer.GetPreviousPosition());

            return new ElseStatement(block, startPostition);
        }

        private IExpression TryParseCondition()
        {
            var expression = TryParseExpression();
            if (expression is null)
                return null;

            return expression;
        }

        private IExpression TryParseCompareCondition(IExpression leftExpression)
        {
            if (!TokenIs(AppType.T_EQUAL) && !TokenIs(AppType.T_NOT_EQUAL) &&
                !TokenIs(AppType.T_GR_OR_EQ) && !TokenIs(AppType.T_GREATER) &&
                !TokenIs(AppType.T_LO_OR_EQ) && !TokenIs(AppType.T_LOWER))
                return null;

            var startPostition = lexer.GetPreviousPosition();

            var comparsionType = Remap(lexer.Token.type);
            lexer.GetNextToken();

            var rigthExpression = TryParseExpression();
            if (rigthExpression is null)
                throw new ExpectedExpressionException(lexer.GetPreviousPosition());

            return new CompareCondition(leftExpression, rigthExpression, comparsionType, startPostition);
        }

        private IExpression TryParseCheckTypeCondition(IExpression leftExpression)
        {
            if (!CheckAndConsume(AppType.T_IS))
                return null;

            if (!TokenIs(AppType.T_FLOAT_TYPE) &&
                !TokenIs(AppType.T_INT_TYPE) &&
                !TokenIs(AppType.T_STRING_TYPE))
                throw new NoValueTypeException(lexer.GetPreviousPosition());

            var startPostition = lexer.GetPreviousPosition();
            var valueType = Remap(lexer.Token.type);
            lexer.GetNextToken();

            return new CheckTypeCondition(leftExpression, valueType, startPostition);
        }

        private IStatement TryParseWhileStatement()
        {
            if (!CheckAndConsume(AppType.T_WHILE))
                return null;
            var startPostition = lexer.GetPreviousPosition();
            var condAndBlock = ParseCondAndBlock();

            return new WhileStatement(condAndBlock.Item1, condAndBlock.Item2, startPostition);
        }

        private IStatement TryParseAssignOrFunCallStatement()
        {
            var mutable = CheckAndConsume(AppType.T_VAR);
            if (!TokenIs(AppType.T_IDENTIFIER) && mutable)
                throw UnexpectedToken(AppType.T_IDENTIFIER);
            else if (!TokenIs(AppType.T_IDENTIFIER))
                return null;

            var identifier = lexer.Token.value;
            var startPostition = lexer.GetPreviousPosition();
            lexer.GetNextToken();

            IStatement result;
            if (!mutable)
                result = TryParseRestOfAssignStatement(identifier.GetString(), mutable, startPostition) ?? TryParseRestOfFunCall(identifier.GetString(), startPostition);
            else
                result = TryParseRestOfAssignStatement(identifier.GetString(), mutable, startPostition);

            if (result is null)
                throw new AloneIdentifierException(startPostition);


            CheckSemiColon();

            return result;
        }


        private IStatement TryParseRestOfAssignStatement(string identifier, bool mutable, Position position)
        {
            if (!TokenIs(AppType.T_ASSIGN))
                return null;
            AppType assignType = CurrentTokenType;
            lexer.GetNextToken();

            var rightValue = TryParseExpression();
            if (rightValue is null)
                throw new ExpectedExpressionException(lexer.GetPreviousPosition());

            return new AssignStatement(identifier, rightValue, assignType, mutable, position);
        }

        private FunCall TryParseRestOfFunCall(string name, Position position)
        {
            if (!CheckAndConsume(AppType.T_LPAREN))
                return null;
            var parameters = ParseCallParameters();

            CheckAndConsumeOrException(AppType.T_RPAREN);

            return new FunCall(name, parameters, position);
        }

        private IList<CallParameter> ParseCallParameters()
        {
            var parameterList = new List<CallParameter>();
            var parameter = TryParseCallParameter();
            if (parameter is null)
                return parameterList;

            parameterList.Add(parameter);
            while (CheckAndConsume(AppType.T_COMMA))
            {
                parameter = TryParseCallParameter();
                if (parameter is null)
                    throw new NoParameterException(lexer.GetPreviousPosition());
                parameterList.Add(parameter);

            }
            return parameterList;
        }

        private CallParameter TryParseCallParameter()
        {
            var expression = TryParseExpression();
            if (expression is null)
                return null;
            return new CallParameter(expression);
        }

        private IStatement TryParsePatternMatchingStatement()
        {
            if (!CheckAndConsume(AppType.T_MATCH))
                return null;
            var startPostition = lexer.GetPreviousPosition();
            TokenIsOrException(AppType.T_IDENTIFIER);

            var identifier = lexer.Token.value.GetString();
            lexer.GetNextToken();

            CheckAndConsumeOrException(AppType.T_ASSIGN);

            var expression = TryParseExpression();

            if (expression is null)
                throw new ExpectedExpressionException(lexer.GetPreviousPosition());

            CheckAndConsumeOrException(AppType.T_COLON);

            var caseList = new List<CaseStatement>();
            var caseStatement = TryParseCaseStatement();

            if (caseStatement is null)
                throw new NoCaseStatementException(lexer.GetPreviousPosition());

            while (caseStatement is not null)
            {
                caseList.Add(caseStatement);
                caseStatement = TryParseCaseStatement();
            }

            var elseStatement = TryParseElseStatement();

            return new PatternMatchingStatement(identifier, expression, caseList, elseStatement, startPostition);
        }

        private CaseStatement TryParseCaseStatement()
        {
            if (!CheckAndConsume(AppType.T_CASE))
                return null;
            var startPostition = lexer.GetPreviousPosition();
            var condAndBlock = ParseCondAndBlock();

            return new CaseStatement(condAndBlock.Item1, condAndBlock.Item2, startPostition);
        }

        private IExpression TryParseExpression()
        {
            var leftAddExpr = TryParseAddExpression();
            AppType operationType;
            if (leftAddExpr is null)
                return null;
            var startPostition = lexer.GetPreviousPosition();
            var boolExpr = TryParseCheckTypeCondition(leftAddExpr) ?? TryParseCompareCondition(leftAddExpr);
            if (boolExpr is not null)
                return boolExpr;
            while (TokenIs(AppType.T_PLUS) || TokenIs(AppType.T_MINUS))
            {
                operationType = CurrentTokenType;
                lexer.GetNextToken();
                var rightAddExpr = TryParseAddExpression();
                if (rightAddExpr is null)
                    throw new ExpectedExpressionException(lexer.GetPreviousPosition());
                if (operationType == AppType.T_MINUS)
                {
                    if (leftAddExpr.GetType() == typeof(StringExpression))
                        throw new IllegalOperationException(lexer.GetPreviousPosition());
                    leftAddExpr = new SubOperationExpression(leftAddExpr, rightAddExpr, startPostition);
                }
                else if (operationType == AppType.T_PLUS)
                    leftAddExpr = new AddOperationExpression(leftAddExpr, rightAddExpr, startPostition);
            }
            return leftAddExpr;

        }

        private IExpression TryParseAddExpression()
        {

            var leftAddExpr = TryParseMulExpression();
            AppType operationType;
            if (leftAddExpr is null)
                return null;
            var startPostition = lexer.GetPreviousPosition();
            while (TokenIs(AppType.T_MODULO) || TokenIs(AppType.T_MULTIPLICATION) || TokenIs(AppType.T_DIVISION))
            {
                operationType = CurrentTokenType;
                lexer.GetNextToken();
                var rightAddExpr = TryParseMulExpression();
                if (rightAddExpr is null)
                    throw new ExpectedExpressionException(lexer.GetPreviousPosition());
                if (operationType == AppType.T_MODULO)
                    leftAddExpr = new ModOperationExpression(leftAddExpr, rightAddExpr, startPostition);
                else if (operationType == AppType.T_MULTIPLICATION)
                    leftAddExpr = new MulOperationExpression(leftAddExpr, rightAddExpr, startPostition);
                else if (operationType == AppType.T_DIVISION)
                    leftAddExpr = new DivOperationExpression(leftAddExpr, rightAddExpr, startPostition);
            }
            return leftAddExpr;

        }

        private IExpression TryParseMulExpression()
        {
            var startPostition = lexer.GetPreviousPosition();
            bool negate = CheckAndConsume(AppType.T_MINUS);
            IExpression expression = TryParseValueRefOrFunCallExpression()
                ?? TryParseNumericExpression()
                ?? TryParseBracketExpression()
                ?? TryParseStringExpression();
            if (negate && expression is null)
                throw new AloneNegateException(lexer.GetPreviousPosition());
            if (negate)
                return new NegateExpression(expression, startPostition);
            return expression;
        }

        private IExpression TryParseNumericExpression()
        {
            var startPostition = lexer.GetPreviousPosition();
            IExpression numericExpression = null;
            if (TokenIs(AppType.T_INT))
                numericExpression = new NumericExpression(lexer.Token.value.GetInt(), startPostition);
            else if (TokenIs(AppType.T_FLOAT))
                numericExpression = new NumericExpression(lexer.Token.value.GetFloat(), startPostition);
            else
                return numericExpression;

            lexer.GetNextToken();
            return numericExpression;
        }

        private IExpression TryParseStringExpression()
        {
            if (!TokenIs(AppType.T_STRING))
                return null;
            var startPostition = lexer.GetPreviousPosition();
            var value = lexer.Token.value.GetString();
            lexer.GetNextToken();
            return new StringExpression(value, startPostition);
        }

        private IExpression TryParseValueRefOrFunCallExpression()
        {
            if (!TokenIs(AppType.T_IDENTIFIER))
                return null;
            var startPostition = lexer.GetPreviousPosition();
            var name = lexer.Token.value.GetString();
            lexer.GetNextToken();
            
            var funCall = TryParseRestOfFunCall(name, startPostition);
            if (funCall is not null)
                return funCall;
            return new ValueRef(name, startPostition);
        }

        private IExpression TryParseBracketExpression()
        {
            var startPostition = lexer.GetPreviousPosition();
            if (!CheckAndConsume(AppType.T_LPAREN))
                return null;
            var expression = TryParseExpression();
            if (expression is null)
                throw new ExpectedExpressionException(lexer.GetPreviousPosition());
            if (expression.GetType() == typeof(StringExpression))
                throw new IllegalOperationException(lexer.GetPreviousPosition());
            CheckAndConsumeOrException(AppType.T_RPAREN);
            return new BracketExpression(expression, startPostition);
        }

        private void InitTokenTypeDir()
        {
            this.tokenTypeDir = new Dictionary<TokenType, AppType>();
            this.tokenTypeDir.Add(TokenType.T_INT, AppType.T_INT);
            this.tokenTypeDir.Add(TokenType.T_FLOAT, AppType.T_FLOAT);
            this.tokenTypeDir.Add(TokenType.T_STRING, AppType.T_STRING);
            this.tokenTypeDir.Add(TokenType.T_IDENTIFIER, AppType.T_IDENTIFIER);
            this.tokenTypeDir.Add(TokenType.T_INT_TYPE, AppType.T_INT_TYPE);
            this.tokenTypeDir.Add(TokenType.T_FLOAT_TYPE, AppType.T_FLOAT_TYPE);
            this.tokenTypeDir.Add(TokenType.T_STRING_TYPE, AppType.T_STRING_TYPE);
            this.tokenTypeDir.Add(TokenType.T_VOID_TYPE, AppType.T_VOID_TYPE);
            this.tokenTypeDir.Add(TokenType.T_EQUAL, AppType.T_EQUAL);
            this.tokenTypeDir.Add(TokenType.T_NOT_EQUAL, AppType.T_NOT_EQUAL);
            this.tokenTypeDir.Add(TokenType.T_GR_OR_EQ, AppType.T_GR_OR_EQ);
            this.tokenTypeDir.Add(TokenType.T_GREATER, AppType.T_GREATER);
            this.tokenTypeDir.Add(TokenType.T_LO_OR_EQ, AppType.T_LO_OR_EQ);
            this.tokenTypeDir.Add(TokenType.T_LOWER, AppType.T_LOWER);
            this.tokenTypeDir.Add(TokenType.T_VAR, AppType.T_VAR);
            this.tokenTypeDir.Add(TokenType.T_ASSIGN, AppType.T_ASSIGN);
            this.tokenTypeDir.Add(TokenType.T_LPAREN, AppType.T_LPAREN);
            this.tokenTypeDir.Add(TokenType.T_RPAREN, AppType.T_RPAREN);
            this.tokenTypeDir.Add(TokenType.T_MULTIPLICATION, AppType.T_MULTIPLICATION);
            this.tokenTypeDir.Add(TokenType.T_DIVISION, AppType.T_DIVISION);
            this.tokenTypeDir.Add(TokenType.T_MODULO, AppType.T_MODULO);
            this.tokenTypeDir.Add(TokenType.T_PLUS, AppType.T_PLUS);
            this.tokenTypeDir.Add(TokenType.T_MINUS, AppType.T_MINUS);
            this.tokenTypeDir.Add(TokenType.T_IS, AppType.T_IS);
            this.tokenTypeDir.Add(TokenType.T_LBRACKET, AppType.T_LBRACKET);
            this.tokenTypeDir.Add(TokenType.T_RBRACKET, AppType.T_RBRACKET);
            this.tokenTypeDir.Add(TokenType.T_IF, AppType.T_IF);
            this.tokenTypeDir.Add(TokenType.T_ELIF, AppType.T_ELIF);
            this.tokenTypeDir.Add(TokenType.T_ELSE, AppType.T_ELSE);
            this.tokenTypeDir.Add(TokenType.T_CASE, AppType.T_CASE);
            this.tokenTypeDir.Add(TokenType.T_WHILE, AppType.T_WHILE);
            this.tokenTypeDir.Add(TokenType.T_RETURN, AppType.T_RETURN);
            this.tokenTypeDir.Add(TokenType.T_SEMICOLON, AppType.T_SEMICOLON);
            this.tokenTypeDir.Add(TokenType.T_UNKNOWN, AppType.T_UNKNOWN);
            this.tokenTypeDir.Add(TokenType.T_COMMA, AppType.T_COMMA);
            this.tokenTypeDir.Add(TokenType.T_ETX, AppType.T_ETX);
            this.tokenTypeDir.Add(TokenType.T_MATCH, AppType.T_MATCH);
            this.tokenTypeDir.Add(TokenType.T_COLON, AppType.T_COLON);
        }
    }
}
