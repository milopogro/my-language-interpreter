﻿using OneOf;
using System;
using System.Collections.Generic;
using System.Text;

namespace TKOM
{
    public class Lexer : ILexer
    {
        public Lexer(ISource source)
        {
            this.source = source;
            currentCharacter = source.GetNextChar();
            keyWords = new Dictionary<string, TokenType>();
            singleSymbolDict = new Dictionary<char, TokenType>();
            doubleSymbolDict = new Dictionary<string, TokenType>();
            InitScanner();
        }

        private readonly Dictionary<string, TokenType> keyWords;

        private Token currentToken;

        private char currentCharacter;

        private Position previousPosition;

        private readonly ISource source;

        private readonly Dictionary<char, TokenType> singleSymbolDict;

        private readonly Dictionary<string, TokenType> doubleSymbolDict;

        public Token Token
        {
            get { return currentToken; }
        }

        private Token CreateToken(TokenType type, OneOf<int, float, string> value, uint startColumn)
        {
            return new Token(type, value, new Position(source.LineNumber, startColumn));
        }

        public Position GetPosition()
        {
            return new Position(source.LineNumber, source.ColumnNumber);
        }

        public Position GetPreviousPosition()
        {
            return previousPosition;
        }

        public Token GetNextToken()
        {
            previousPosition = GetPosition();
            while (SkipWhites() || SkipComment()) ;

            currentToken = TryBuildNumber() ??
                           TryBuildString() ??
                           TryBuildIdentifier() ??
                           TrySingleSymbol() ??
                           TryDoubleSymbol() ??
                           TryBuildNumber() ??
                           TryETX() ??
                           CreateToken(TokenType.T_UNKNOWN, "", source.ColumnNumber);

            if (currentToken.type == TokenType.T_UNKNOWN)
                throw new UnknownTokenException(new Position(source.LineNumber, source.ColumnNumber));

            return currentToken;
        }

        private Token? TryBuildNumber()
        {
            if (!Char.IsDigit(currentCharacter))
                return null;

            var startColumn = source.ColumnNumber;
            int int_part = 0;
            int_part += (int)(currentCharacter - '0');
            GetNextCharacter();
            while (Char.IsDigit(currentCharacter))
            {
                var digit = (int)(currentCharacter - '0');
                if ((int_part < 214748364) || (int_part == 214748364 && digit <= 7))
                    int_part = int_part * 10 + digit;
                else
                    throw new IntOverflowException(new Position(source.LineNumber, source.ColumnNumber));

                GetNextCharacter();
            }
            if (currentCharacter == '.')
            {
                GetNextCharacter();
                int fraction_part = 0;
                int decimal_places = 0;
                while (Char.IsDigit(currentCharacter))
                {
                    var digit = (int)(currentCharacter - '0');
                    if ((fraction_part < 214748364) || (fraction_part == 214748364 && digit < 7))
                        fraction_part = fraction_part * 10 + digit;
                    else
                        throw new IntOverflowException(new Position(source.LineNumber, source.ColumnNumber));

                    decimal_places++;
                    GetNextCharacter();
                }

                if(currentCharacter == '.')
                    throw new UnknownTokenException(new Position(source.LineNumber, source.ColumnNumber));

                float value = int_part + fraction_part / (float)Math.Pow(10, decimal_places);
                return CreateToken(TokenType.T_FLOAT, value, startColumn);
            }
            else
            {
                return CreateToken(TokenType.T_INT, int_part, startColumn);
            }
        }

        private Token? TryETX()
        {
            if (currentToken.type == TokenType.T_ETX)
                return currentToken;

            if (currentCharacter == '\x03')
            {
                return CreateToken(TokenType.T_ETX, '\x03', source.ColumnNumber);
            }
            return null;
        }

        private Token? TrySingleSymbol()
        {
            TokenType type;

            if (singleSymbolDict.TryGetValue(currentCharacter, out type))
            {
                GetNextCharacter();
                return CreateToken(type, currentCharacter, source.ColumnNumber - 1); ;
            }
            return null;
        }


        private Token? TryDoubleSymbol()
        {
            TokenType type;

            if (doubleSymbolDict.TryGetValue(currentCharacter.ToString(), out type))
            {
                var token = CreateToken(type, currentCharacter.ToString(), source.ColumnNumber);
                GetNextCharacter();

                if (doubleSymbolDict.TryGetValue(token.value.GetString() + currentCharacter.ToString(), out type))
                {
                    token = CreateToken(type, token.value.GetString() + currentCharacter, source.ColumnNumber - 1);
                    GetNextCharacter();
                }
                return token;
            }
            return null;
        }

        private string StringEscaping()
        {
            StringBuilder value = new();
            GetNextCharacter();
            if (currentCharacter == '"')
            {
                value.Append('"');
                GetNextCharacter();
            }
            else
            {
                value.Append('!');
                if (currentCharacter != '!')
                {
                    if (currentCharacter == '\x03')
                        throw new ETXInStringException(new Position(source.LineNumber, source.ColumnNumber - 1));
                    value.Append(currentCharacter);
                    GetNextCharacter();
                }
            }
            return value.ToString();
        }

        private Token? TryBuildString()
        {
            if (currentCharacter == '"')
            {
                var startColumn = source.ColumnNumber;
                StringBuilder value = new();
                GetNextCharacter();
                while (currentCharacter != '"')
                {
                    if (currentCharacter == '\x03')
                        throw new ETXInStringException(new Position(source.LineNumber, source.ColumnNumber - 1));
                    if (currentCharacter == '!')
                    {
                        value.Append(StringEscaping());
                    }
                    else
                    {
                        value.Append(currentCharacter);
                        GetNextCharacter();

                    }
                }
                GetNextCharacter();
                return CreateToken(TokenType.T_STRING, value.ToString(), startColumn);
            }
            return null;
        }

        private Token? TryBuildIdentifier()
        {
            if (Char.IsLetter(currentCharacter))
            {
                var startColumn = source.ColumnNumber;
                StringBuilder value = new();
                value.Append(currentCharacter);
                GetNextCharacter();
                while (Char.IsLetterOrDigit(currentCharacter))
                {
                    value.Append(currentCharacter);
                    GetNextCharacter();
                }
                TokenType type;

                if (keyWords.TryGetValue(value.ToString(), out type))
                    return CreateToken(type, value.ToString(), startColumn);

                return CreateToken(TokenType.T_IDENTIFIER, value.ToString(), startColumn);
            }
            return null;
        }

        private bool SkipWhites()
        {
            if (Char.IsWhiteSpace(currentCharacter))
            {
                GetNextCharacter();
                return true;
            }
            return false;
        }

        private bool SkipComment()
        {
            if (currentCharacter == '#')
            {
                var startLineNumber = source.LineNumber;
                while (source.LineNumber == startLineNumber && currentCharacter != '\x03')
                    GetNextCharacter();
                return true;
            }
            return false;
        }

        private void GetNextCharacter()
        {
            currentCharacter = source.GetNextChar(); ;
        }

        public void InitScanner()
        {
            keyWords.Add("while", TokenType.T_WHILE);
            keyWords.Add("int", TokenType.T_INT_TYPE);
            keyWords.Add("float", TokenType.T_FLOAT_TYPE);
            keyWords.Add("string", TokenType.T_STRING_TYPE);
            keyWords.Add("void", TokenType.T_VOID_TYPE);
            keyWords.Add("var", TokenType.T_VAR);
            keyWords.Add("if", TokenType.T_IF);
            keyWords.Add("else", TokenType.T_ELSE);
            keyWords.Add("elif", TokenType.T_ELIF);
            keyWords.Add("match", TokenType.T_MATCH);
            keyWords.Add("case", TokenType.T_CASE);
            keyWords.Add("return", TokenType.T_RETURN);
            keyWords.Add("is", TokenType.T_IS);

            singleSymbolDict.Add('*', TokenType.T_MULTIPLICATION);
            singleSymbolDict.Add('/', TokenType.T_DIVISION);
            singleSymbolDict.Add('%', TokenType.T_MODULO);
            singleSymbolDict.Add(';', TokenType.T_SEMICOLON);
            singleSymbolDict.Add('(', TokenType.T_LPAREN);
            singleSymbolDict.Add(')', TokenType.T_RPAREN);
            singleSymbolDict.Add('{', TokenType.T_LBRACKET);
            singleSymbolDict.Add('}', TokenType.T_RBRACKET);
            singleSymbolDict.Add(',', TokenType.T_COMMA);
            singleSymbolDict.Add(':', TokenType.T_COLON);

            doubleSymbolDict.Add("=", TokenType.T_ASSIGN);
            doubleSymbolDict.Add("-", TokenType.T_MINUS);
            doubleSymbolDict.Add("+", TokenType.T_PLUS);
            doubleSymbolDict.Add(">", TokenType.T_GREATER);
            doubleSymbolDict.Add("<", TokenType.T_LOWER);
            doubleSymbolDict.Add("!", TokenType.T_UNKNOWN);
            doubleSymbolDict.Add("==", TokenType.T_EQUAL);
            doubleSymbolDict.Add("!=", TokenType.T_NOT_EQUAL);
            doubleSymbolDict.Add("<=", TokenType.T_LO_OR_EQ);
            doubleSymbolDict.Add(">=", TokenType.T_GR_OR_EQ);
        }
    }
}
