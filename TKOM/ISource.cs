﻿namespace TKOM
{
    public interface ISource
    {
        uint LineNumber{ get; }

        uint ColumnNumber { get; }

        char GetNextChar();
    }
}
