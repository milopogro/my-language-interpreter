﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKOM
{
    public interface IErrorHandler
    {
        public void HandleException(ReadableException e);
    }
}
