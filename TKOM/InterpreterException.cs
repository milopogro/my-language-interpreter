﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKOM
{

    public class InterpreterException : ReadableException
    {
        public InterpreterException(Position position) : base(position)
        {
        }
    }

    public class NoMainException : InterpreterException
    {
        public NoMainException(Position position) : base(position)
        {
        }
    }

    public class UndeclaredVariableException : InterpreterException
    {
        public UndeclaredVariableException(Position position) : base(position)
        {
        }
    }

    public class WrongNumberOfParametersException : InterpreterException
    {
        public WrongNumberOfParametersException(Position position) : base(position)
        {
        }
    }

    public class TryingToAddExistingVariableException : InterpreterException
    {
        public TryingToAddExistingVariableException(Position position) : base(position)
        {
        }
    }

    public class TryingToEditUnmutableVariableException : InterpreterException
    {
        public TryingToEditUnmutableVariableException(Position position) : base(position)
        {
        }
    }

    public class FunctionNotDeclaredException : InterpreterException
    {
        public FunctionNotDeclaredException(Position position) : base(position)
        {
        }
    }

    public class IllegalValueOperationException : ReadableException
    {
        public IllegalValueOperationException() : base(new Position(0,0))
        {
        }
    }

    public class NoReturnException : InterpreterException
    {
        public NoReturnException(Position position) : base(position)
        {
        }
    }

}
