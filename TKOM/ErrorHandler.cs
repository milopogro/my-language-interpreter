﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKOM
{
    public class ErrorHandler : IErrorHandler
    {
        private readonly IPrinter printer;
        public ErrorHandler(IPrinter printer)
        {
            this.printer = printer;
        }

        public void HandleException(ReadableException e)
        {
            printer.Print(e.GetType().ToString());
            printer.Print("Line:"+e.position.lineNumber.ToString()+
                " Column:" + e.position.column.ToString());
        }
    }
}
