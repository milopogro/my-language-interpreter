﻿using System.IO;

namespace TKOM //oblsuga linii i kolumny w zrodle
{
    public class StringSource : ISource
    {
        public StringSource(string content)
        {
            this.content = content;
            index = -1;
            ColumnNumber = 0;
            LineNumber = 1;
        }

        readonly private string content;

        private int index;

        private char CurrentChar;

        public uint LineNumber { get; private set; }

        public uint ColumnNumber { get; private set ; }

        public void ReadNextChar()
        {
            if (index == content.Length - 1)
                CurrentChar = '\x03';
            else
            {
                ColumnNumber++;
                index++;
                CurrentChar = content[index];
            }

        }

        public char GetNextChar()
        {
            ReadNextChar();
            CheckIfNewLine();
            return CurrentChar;
        }

        private void CheckIfNewLine()
        {
            if (CurrentChar == '\n')
            {
                ReadNextChar();
                if (CurrentChar == '\r')
                {
                    LineNumber++;
                    ColumnNumber = 1;
                }
                else
                {
                    index--;
                    ColumnNumber = 1;
                    LineNumber++;
                }
                CurrentChar = '\n';
            }
            else if (CurrentChar == '\r')
            {
                ReadNextChar();
                if (CurrentChar == '\n')
                {
                    LineNumber++;
                    ColumnNumber = 1;
                }
                else
                {
                    index--;
                    ColumnNumber = 1;
                    LineNumber++;
                }
                CurrentChar = '\n';
            }
        }
    }

    public class FileSource : ISource
    {
        public FileSource(string path)
        {
            streamReader = new StreamReader(path);
            ColumnNumber = 0;
            LineNumber = 1;
        }

        readonly private StreamReader streamReader;

        public char CurrentChar;

        public uint LineNumber { get; private set; }

        public uint ColumnNumber { get; private set; }

        public void ReadNextChar()
        {
            var result = streamReader.Peek();
            if (result == -1)
                CurrentChar = '\x03';
            else
            {
                ColumnNumber++;
                CurrentChar = (char)streamReader.Read();
            }     
        }

        public char GetNextChar()
        {
            ReadNextChar();
            CheckIfNewLine();
            return CurrentChar;
        }

        private void CheckIfNewLine()
        {
            if (CurrentChar == '\n')
            {
                ReadNextChar();
                if (CurrentChar == '\r')
                {
                    ReadNextChar();
                    LineNumber++;
                    ColumnNumber = 1;
                }
                else
                {
                    ColumnNumber = 1;
                    LineNumber++;
                }
            }
            else if (CurrentChar == '\r')
            {
                ReadNextChar();
                if (CurrentChar == '\n')
                {
                    ReadNextChar();
                    LineNumber++;
                    ColumnNumber = 1;
                }
                else
                {
                    ColumnNumber = 1;
                    LineNumber++;
                }
            }
        }
    }
}
