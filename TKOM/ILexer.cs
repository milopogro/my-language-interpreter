﻿using OneOf;

namespace TKOM
{
    public struct Position
    {
        public Position(uint lineNumber, uint column)
        {
            this.lineNumber = lineNumber;
            this.column = column;
        }
        public uint lineNumber;
        public uint column;
    }

    public enum TokenType
    {
        T_INT,
        T_FLOAT,
        T_STRING,
        T_IDENTIFIER,
        T_INT_TYPE,
        T_FLOAT_TYPE,
        T_STRING_TYPE,
        T_VOID_TYPE,
        T_EQUAL,
        T_NOT_EQUAL,
        T_GR_OR_EQ,
        T_GREATER,
        T_LO_OR_EQ,
        T_LOWER,
        T_VAR,
        T_ASSIGN,
        T_ASSIGN_PLUS,
        T_ASSIGN_MINUS,
        T_LPAREN,
        T_RPAREN,
        T_MULTIPLICATION,
        T_DIVISION,
        T_MODULO,
        T_PLUS,
        T_MINUS,
        T_IS,
        T_LBRACKET,
        T_RBRACKET,
        T_IF,
        T_ELIF,
        T_ELSE,
        T_CASE,
        T_WHILE,
        T_RETURN,
        T_SEMICOLON,
        T_UNKNOWN,
        T_COMMA,
        T_ETX,
        T_MATCH,
        T_COLON
    }

    public struct Token
    {
        public Token(TokenType type, OneOf<int, float, string> value, Position position)
        {
            this.type = type;
            this.value = new Variant(value);
            this.position = position;
        }
        public TokenType type;
        public Variant value;
        public Position position;
    }

    public enum ValueType
    {
        INTTYPE,
        FLOATTYPE,
        STRINGTYPE,
        UNKNOWN
    }

    public class Variant
    {
        public Variant(OneOf<int, float, string> value) { this.value = value; }
        private readonly OneOf<int, float, string> value;
        public int GetInt() { return value.AsT0; }
        public float GetFloat() { return value.AsT1; }
        public string GetString() { return value.AsT2; }
        public ValueType GetValueType()
        {
            var type = value.Index;
            return type switch
            {
                0 => ValueType.INTTYPE,
                1 => ValueType.FLOATTYPE,
                2 => ValueType.STRINGTYPE,
                _ => ValueType.UNKNOWN,
            };
        }
    }

    public interface ILexer
    {
        Token GetNextToken();
        Position GetPosition();
        Position GetPreviousPosition();
        Token Token { get; }
        void InitScanner();
    }
}
