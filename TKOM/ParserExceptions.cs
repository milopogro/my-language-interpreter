﻿using System;

namespace TKOM
{
    public class ParserException : ReadableException
    {
        public ParserException(Position position) : base(position)
        {
        }
    }

    public class NoFunctionException : ParserException
    {
        public NoFunctionException(Position position) : base(position)
        {
        }
    }

    public class ExpectedExpressionException : ParserException
    {
        public ExpectedExpressionException(Position position) : base(position)
        {
        }
    }

    public class NoParameterException: ParserException
    {
        public NoParameterException(Position position) : base(position)
        {
        }

    }

    public class NoSemiColonException : ParserException
    {
        public NoSemiColonException(Position position) : base(position)
        {
        }

    }

    public class NoValueTypeException : ParserException
    {
        public NoValueTypeException(Position position) : base(position)
        {
        }

    }

    public class NoConditionException : ParserException
    {
        public NoConditionException(Position position) : base(position)
        {
        }

    }

    public class NoBlockException : ParserException
    {
        public NoBlockException(Position position) : base(position)
        {
        }
    }


    public class UnknownTokenTypeException : ParserException
    {
        public UnknownTokenTypeException(Position position) : base(position)
        {
        }
    }

    public class AloneNegateException : ParserException
    {
        public AloneNegateException(Position position) : base(position)
        {
        }

    }

    public class AloneIdentifierException : ParserException
    {
        public AloneIdentifierException(Position position) : base(position)
        {
        }

    }

    public class IllegalOperationException : ParserException
    {
        public IllegalOperationException(Position position) : base(position)
        {
        }

    }

    public class NoCaseStatementException : ParserException
    {
        public NoCaseStatementException(Position position) : base(position)
        {
        }
    }

    public class UnexpectedTokenException : ParserException
    {
        public AppType expectedToken;
        public AppType givenToken;

        public UnexpectedTokenException(Position position, AppType expectedToken, AppType givenToken) : base(position)
        {
            this.expectedToken = expectedToken;
            this.givenToken = givenToken;
        }
    }
}
