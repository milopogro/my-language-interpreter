﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKOM
{
    public class PrinterMock : IPrinter
    {
        public List<string> output = new();
        public void Print(string text)
        {
            output.Add(text);
        }
    }
}
