﻿using System;

namespace TKOM
{
    public class ReadableException : Exception
    {
        public Position position;

        public ReadableException(Position position)
        {
            this.position = position;
        }

    }

    public class LexerException : ReadableException
    {
        public LexerException(Position position) : base(position)
        {
        }
    }

    public class ETXInStringException : LexerException
    {
        public ETXInStringException(Position position) : base(position)
        {
        }
    }

    public class UnknownTokenException : LexerException
    {
        public UnknownTokenException(Position position) : base(position)
        {
        }
    }

    public class IntOverflowException : LexerException
    {
        public IntOverflowException(Position position) : base(position)
        {
        }

    }
}
