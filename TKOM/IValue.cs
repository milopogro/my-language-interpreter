﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKOM
{
    public interface IValue
    {
        void Add(IValue value);
        void Sub(IValue value);
        void Mul(IValue value);
        void Div(IValue value);
        void Mod(IValue value);
        void Less(IValue value);
        void LessEqual(IValue value);
        void Greater(IValue value);
        void GreaterEqual(IValue value);
        void Equal(IValue value);
        void NotEqual(IValue value);
        public IValue Result { get; }
        public int ValueAsInt { get; }
        public float ValueAsFloat { get; }
        public string ValueAsString { get; }
        public Variant ValueAsVariant { get; }
        public bool Mutable { get; set;  }
        void CheckType();
        void Negate();
        IValue Clone();
    }

    public class IntValue : IValue
    {
        private readonly int value;

        private IValue result;

        private bool mutable;

        public bool Mutable { get { return mutable; } set { mutable = value; } }

        public IValue Result { get { return result; } }

        public int ValueAsInt { get { return value; } }

        public float ValueAsFloat { get { return value; } }

        public string ValueAsString { get { return value.ToString(); } }

        public Variant ValueAsVariant { get { return new Variant(value); } }

        public IValue Clone() { return (IValue)this.MemberwiseClone(); }

        public IntValue(int initValue, bool mutable = false)
        {
            this.value = initValue;
            this.mutable = mutable;
        }

        public void Add(IValue rValue)
        {
            result = new IntValue(value + rValue.ValueAsInt);
        }

        public void Sub(IValue rValue)
        {
            result = new IntValue(value - rValue.ValueAsInt);
        }

        public void Mul(IValue rValue)
        {
            result = new IntValue(value * rValue.ValueAsInt);
        }

        public void Div(IValue rValue)
        {
            if(rValue.ValueAsInt == 0)
                throw new IllegalValueOperationException();
            result = new IntValue(value / rValue.ValueAsInt);
        }

        public void Mod(IValue rValue)
        {
            if (rValue.ValueAsInt == 0)
                throw new IllegalValueOperationException();
            result = new IntValue(value % rValue.ValueAsInt);
        }

        public void Less(IValue rValue)
        {
            result = new IntValue(Convert.ToInt32(value < rValue.ValueAsInt));
        }

        public void LessEqual(IValue rValue)
        {
            result = new IntValue(Convert.ToInt32(value <= rValue.ValueAsInt));
        }

        public void Greater(IValue rValue)
        {
            result = new IntValue(Convert.ToInt32(value > rValue.ValueAsInt));
        }

        public void GreaterEqual(IValue rValue)
        {
            result = new IntValue(Convert.ToInt32(value >= rValue.ValueAsInt));
        }

        public void Equal(IValue rValue)
        {
            result = new IntValue(Convert.ToInt32(value == rValue.ValueAsInt));
        }

        public void NotEqual(IValue rValue)
        {
            result = new IntValue(Convert.ToInt32(value != rValue.ValueAsInt));
        }

        public void CheckType()
        {
            result = new IntValue(0);
        }

        public void Negate()
        {
            result = new IntValue(-value);
        }
    }

    public class FloatValue : IValue
    {
        private readonly float value;

        private IValue result;

        private bool mutable;

        public bool Mutable { get { return mutable; } set { mutable = value; } }

        public IValue Result { get { return result; } }

        public int ValueAsInt { get { return (int)value; } }

        public float ValueAsFloat { get { return value; } }

        public string ValueAsString { get { return value.ToString(); } }

        public Variant ValueAsVariant { get { return new Variant(value); } }

        public IValue Clone() { return (IValue)this.MemberwiseClone(); }

        public FloatValue(float initValue, bool mutable = false)
        {
            this.value = initValue;
            this.mutable = mutable;
        }

        public void Add(IValue rValue)
        {
            result = new FloatValue(value + rValue.ValueAsFloat);
        }

        public void Sub(IValue rValue)
        {
            result = new FloatValue(value - rValue.ValueAsFloat);
        }

        public void Mul(IValue rValue)
        {
            result = new FloatValue(value * rValue.ValueAsFloat);
        }

        public void Div(IValue rValue)
        {
            if (rValue.ValueAsFloat == 0)
                throw new IllegalValueOperationException();
            result = new FloatValue(value / rValue.ValueAsFloat);
        }

        public void Mod(IValue rValue)
        {
            if (rValue.ValueAsFloat == 0)
                throw new IllegalValueOperationException();
            result = new FloatValue(value % rValue.ValueAsFloat);
        }

        public void Less(IValue rValue)
        {
            result = new IntValue(Convert.ToInt32(value < rValue.ValueAsFloat));
        }

        public void LessEqual(IValue rValue)
        {
            result = new IntValue(Convert.ToInt32(value <= rValue.ValueAsFloat));
        }

        public void Greater(IValue rValue)
        {
            result = new IntValue(Convert.ToInt32(value > rValue.ValueAsFloat));
        }

        public void GreaterEqual(IValue rValue)
        {
            result = new IntValue(Convert.ToInt32(value >= rValue.ValueAsFloat));
        }

        public void Equal(IValue rValue)
        {
            result = new IntValue(Convert.ToInt32(value == rValue.ValueAsFloat));
        }

        public void NotEqual(IValue rValue)
        {
            result = new IntValue(Convert.ToInt32(value != rValue.ValueAsFloat));
        }

        public void CheckType()
        {
            result = new IntValue(1);
        }

        public void Negate()
        {
            result = new FloatValue(-value);
        }
    }

    public class StringValue : IValue
    {
        private readonly string value;

        private IValue result;

        private bool mutable;

        public bool Mutable { get { return mutable; } set { mutable = value; } }

        public IValue Result { get { return result; } }

        public IValue Clone() { return (IValue)this.MemberwiseClone(); }

        public int ValueAsInt 
        {
            get {
                int number;
                if (Int32.TryParse(value, out number))
                    return number;
                return 0;
            } 
        }

        public float ValueAsFloat
        {
            get
            {
                float number;
                if (Single.TryParse(value, out number))
                    return number;
                return 0;
            }
        }

        public string ValueAsString { get { return value.ToString(); } }

        public Variant ValueAsVariant { get { return new Variant(value); } }

        public StringValue(string initValue, bool mutable = false)
        {
            this.value = initValue;
            this.mutable = mutable;
        }

        public void Add(IValue rValue)
        {
            result = new StringValue(value + rValue.ValueAsString);
        }

        public void Sub(IValue rValue)
        {
            throw new IllegalValueOperationException();
        }

        public void Mul(IValue rValue)
        {
            throw new IllegalValueOperationException();
        }

        public void Div(IValue rValue)
        {
            throw new IllegalValueOperationException();
        }

        public void Mod(IValue rValue)
        {
            throw new IllegalValueOperationException();
        }

        public void Less(IValue rValue)
        {
            var status = String.Compare(value, rValue.ValueAsString);
            if(status>0)
                result = new IntValue(Convert.ToInt32(1));
            else
                result = new IntValue(Convert.ToInt32(0));
        }

        public void LessEqual(IValue rValue)
        {
            var status = String.Compare(value, rValue.ValueAsString);
            if (status > 0 || status == 0)
                result = new IntValue(Convert.ToInt32(1));
            else
                result = new IntValue(Convert.ToInt32(0));
        }

        public void Greater(IValue rValue)
        {
            var status = String.Compare(value, rValue.ValueAsString);
            if (status < 0)
                result = new IntValue(Convert.ToInt32(1));
            else
                result = new IntValue(Convert.ToInt32(0));
        }

        public void GreaterEqual(IValue rValue)
        {
            var status = String.Compare(value, rValue.ValueAsString);
            if (status < 0 || status == 0)
                result = new IntValue(Convert.ToInt32(1));
            else
                result = new IntValue(Convert.ToInt32(0));
        }

        public void Equal(IValue rValue)
        {
            var status = String.Compare(value, rValue.ValueAsString);
            if (status == 0)
                result = new IntValue(Convert.ToInt32(1));
            else
                result = new IntValue(Convert.ToInt32(0));
        }

        public void NotEqual(IValue rValue)
        {
            var status = String.Compare(value, rValue.ValueAsString);
            if (status != 0)
                result = new IntValue(Convert.ToInt32(1));
            else
                result = new IntValue(Convert.ToInt32(0));
        }

        public void CheckType()
        {
            result = new IntValue(2);
        }

        public void Negate()
        {
            throw new IllegalValueOperationException();
        }
    }
}
