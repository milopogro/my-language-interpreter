﻿using OneOf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKOM
{
    public class App
    {
        public App(IDictionary<string, IFunction> functionDir)
        {
            this.FunctionDir = functionDir;
            this.position = new Position(0, 0);
        }
        public IDictionary<string, IFunction> FunctionDir { get; set; }
        private readonly Position position;

        public Position Position { get { return position; } }
    }

    public interface IStatement
    {
        public void Accept(IStatementVisitor visitor);
        public Position Position { get; }
    }

    public interface IFunction
    {
        public void Accept(IFunctionVisitor visitor);
        public Position Position { get; }
        public IList<DefParameter> Parameters { get;}
        public string Name { get;}
        public AppType FunctionType { get;}
    }

    public class PrintFunction : IFunction
    {
        private Position position = new Position(0,0);
        private string name = "print";
        private AppType funcitomType = AppType.T_VOID_TYPE;
        private IList<DefParameter> args = new List<DefParameter>() { new DefParameter(false, "toPrint")};

        public IList<DefParameter> Parameters { get { return args; } }
        public Position Position { get { return position; } set { position = value; } }
        public string Name { get { return name; } }
        public AppType FunctionType { get { return funcitomType; } }

        public void Accept(IFunctionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class FunctionDefinition : IFunction
    {
        public FunctionDefinition(string name, AppType functionType, IList<DefParameter> parameters, IList<IStatement> instructions, Position position)
        {
            this.Name = name;
            this.FunctionType = functionType;
            this.Parameters = parameters;
            this.StatementList = instructions;
            this.position = position;
        }

        public string Name { get; set; }
        public AppType FunctionType { get; set; }
        public IList<IStatement> StatementList { get; set; }
        public IList<DefParameter> Parameters { get; set; }
        private readonly Position position;

        public Position Position { get { return position; } }

        public void Accept(IFunctionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class IfStatement : IStatement
    {
        public IExpression condition;
        public IList<IStatement> block;
        public IList<ElifStatement> elifList;
        public ElseStatement elseStatement;
        private readonly Position position;

        public Position Position { get { return position; } }

        public IfStatement(IExpression condition, IList<IStatement> block, IList<ElifStatement> elifList, ElseStatement elseStatement, Position position)
        {
            this.condition = condition;
            this.block = block;
            this.elifList = elifList;
            this.elseStatement = elseStatement;
            this.position = position;
        }

        public void Accept(IStatementVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class ElifStatement : IStatement
    {
        public IExpression condition;
        public IList<IStatement> block;
        private readonly Position position;

        public Position Position { get { return position; } }
        public ElifStatement(IExpression condition, IList<IStatement> block, Position position)
        {
            this.condition = condition;
            this.block = block;
            this.position = position;
        }

        public void Accept(IStatementVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class ElseStatement : IStatement
    {
        public IList<IStatement> block;
        private readonly Position position;

        public Position Position { get { return position; } }
        public ElseStatement(IList<IStatement> block, Position position)
        {
            this.block = block;
            this.position = position;
        }

        public void Accept(IStatementVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class WhileStatement : IStatement
    {
        public IExpression condition;
        public IList<IStatement> block;
        private readonly Position position;

        public Position Position { get { return position; } }

        public WhileStatement(IExpression condition, IList<IStatement> block, Position position)
        {
            this.condition = condition;
            this.block = block;
            this.position = position;
        }

        public void Accept(IStatementVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class AssignStatement : IStatement
    {
        public string identifier;
        public IExpression rightValue;
        public AppType assignType;
        public bool mutable;
        private readonly Position position;

        public Position Position { get { return position; } }

        public AssignStatement(string identifier, IExpression rightValue, AppType assignType, bool mutable, Position position)
        {
            this.identifier = identifier;
            this.rightValue = rightValue;
            this.assignType = assignType;
            this.mutable = mutable;
            this.position = position;
        }

        public void Accept(IStatementVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class PatternMatchingStatement : IStatement
    {
        public string identifier;
        public IExpression expression;
        public IList<CaseStatement> caseList;
        public ElseStatement elseStatement;
        private readonly Position position;

        public Position Position { get { return position; } }

        public PatternMatchingStatement(string identifier, IExpression expression, IList<CaseStatement> caseList, ElseStatement elseStatement, Position position)
        {
            this.identifier = identifier;
            this.expression = expression;
            this.caseList = caseList;
            this.elseStatement = elseStatement;
            this.position = position;
        }

        public void Accept(IStatementVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class ReturnStatement : IStatement
    {
        public IExpression result;
        private readonly Position position;

        public Position Position { get { return position; } }

        public ReturnStatement(IExpression result, Position position)
        {
            this.result = result;
            this.position = position;
        }

        public void Accept(IStatementVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class CaseStatement : IStatement
    {
        public IExpression condition;
        public IList<IStatement> block;
        private readonly Position position;

        public Position Position { get { return position; } }

        public CaseStatement(IExpression condition, IList<IStatement> block, Position position)
        {
            this.condition = condition;
            this.block = block;
            this.position = position;
        }

        public void Accept(IStatementVisitor visitor)
        {
            visitor.Visit(this);
        }
    }



    public class DefParameter
    {
        public DefParameter(bool mutable, string name)
        {
            this.mutable = mutable;
            this.name = name;
        }
        public bool mutable;
        public string name;
    }


    public interface IExpression
    {
        public void Accept(IExpressionVisitor visitor);
        public Position Position { get; }
    }

    public interface IBinaryExpression : IExpression
    {
        public IExpression LeftExpression { get;}
        public IExpression RightExpression { get;}
    }

    public class AddOperationExpression : IBinaryExpression
    {
        public AddOperationExpression(IExpression leftExpression, IExpression rightExpression, Position position)
        {
            this.leftExpression = leftExpression;
            this.rightExpression = rightExpression;
            this.position = position;
        }
        private readonly IExpression leftExpression;
        private readonly IExpression rightExpression;
        private readonly Position position;

        public Position Position { get { return position; } }
        public IExpression LeftExpression { get { return leftExpression; } }
        public IExpression RightExpression { get { return rightExpression; } }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class SubOperationExpression : IBinaryExpression
    {
        public SubOperationExpression(IExpression leftExpression, IExpression rightExpression, Position position)
        {
            this.leftExpression = leftExpression;
            this.rightExpression = rightExpression;
            this.position = position;
        }
        private readonly IExpression leftExpression;
        private readonly IExpression rightExpression;
        private readonly Position position;

        public Position Position { get { return position; } }
        public IExpression LeftExpression { get { return leftExpression; } }
        public IExpression RightExpression { get { return rightExpression; } }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class MulOperationExpression : IBinaryExpression
    {
        public MulOperationExpression(IExpression leftExpression, IExpression rightExpression, Position position)
        {
            this.leftExpression = leftExpression;
            this.rightExpression = rightExpression;
            this.position = position;
        }
        private readonly IExpression leftExpression;
        private readonly IExpression rightExpression;
        private readonly Position position;

        public Position Position { get { return position; } }
        public IExpression LeftExpression { get { return leftExpression; } }
        public IExpression RightExpression { get { return rightExpression; } }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class DivOperationExpression : IBinaryExpression
    {
        public DivOperationExpression(IExpression leftExpression, IExpression rightExpression, Position position)
        {
            this.leftExpression = leftExpression;
            this.rightExpression = rightExpression;
            this.position = position;
        }
        private readonly IExpression leftExpression;
        private readonly IExpression rightExpression;
        private readonly Position position;

        public Position Position { get { return position; } }
        public IExpression LeftExpression { get { return leftExpression; } }
        public IExpression RightExpression { get { return rightExpression; } }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class ModOperationExpression : IBinaryExpression
    {
        public ModOperationExpression(IExpression leftExpression, IExpression rightExpression, Position position)
        {
            this.leftExpression = leftExpression;
            this.rightExpression = rightExpression;
            this.position = position;
        }
        private readonly IExpression leftExpression;
        private readonly IExpression rightExpression;
        private readonly Position position;

        public Position Position { get { return position; } }
        public IExpression LeftExpression { get { return leftExpression; } }
        public IExpression RightExpression { get { return rightExpression; } }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class NegateExpression : IExpression
    {
        public NegateExpression(IExpression expression, Position position)
        {
            this.expression = expression;
            this.position = position;
        }
        private readonly IExpression expression;
        private readonly Position position;

        public Position Position { get { return position; } }
        public IExpression Expression { get { return expression; } }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class NumericExpression : IExpression
    {
        public NumericExpression(int value, Position position)
        {
            this.value = new Variant(value);
            this.position = position;
        }
        public NumericExpression(float value, Position position)
        {
            this.value = new Variant(value);
            this.position = position;
        }

        private readonly Variant value;
        private readonly Position position;

        public Position Position { get { return position; } }
        public Variant Value { get { return value; } }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class StringExpression : IExpression
    {
        public StringExpression(string value, Position position)
        {
            this.value = value;
            this.position = position;
        }
        private readonly string value;
        private readonly Position position;

        public Position Position { get { return position; } }
        public string Value { get { return value; } }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class BracketExpression : IExpression
    {
        public BracketExpression(IExpression expression, Position position)
        {
            this.expression = expression;
            this.position = position;
        }

        private readonly IExpression expression;
        private readonly Position position;

        public Position Position { get { return position; } }
        public IExpression Expression { get { return expression; } }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class ValueRef : IExpression
    {
        public ValueRef(string name, Position position)
        {
            this.name = name;
            this.position = position;
        }

        private readonly string name;
        private readonly Position position;

        public Position Position { get { return position; } }
        public string Name { get { return name; } }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class FunCall : IExpression, IStatement
    {
        public FunCall(string funName, IList<CallParameter> parameterList, Position position)
        {
            this.funName = funName;
            this.parameterList = parameterList;
            this.position = position;
        }

        private readonly string funName;
        private readonly IList<CallParameter> parameterList;
        private readonly Position position;

        public string FunName { get { return funName; } }
        public IList<CallParameter> ParameterList { get { return parameterList; } }
        public Position Position { get { return position; } }

        public void Accept(IStatementVisitor visitor)
        {
            visitor.Visit(this);
        }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class CompareCondition : IBinaryExpression
    {
        private readonly AppType compareOperation;
        private readonly IExpression leftExpression;
        private readonly IExpression rightExpression;
        private readonly Position position;

        public Position Position { get { return position; } }
        public IExpression LeftExpression { get { return leftExpression; } }
        public IExpression RightExpression { get { return rightExpression; } }
        public AppType CompareOperation { get { return compareOperation; } }

        public CompareCondition(IExpression leftExpression, IExpression rightExpression, AppType compareOperation, Position position)
        {
            this.leftExpression = leftExpression;
            this.rightExpression = rightExpression;
            this.compareOperation = compareOperation;
            this.position = position;
        }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class CheckTypeCondition : IExpression
    {
        private readonly IExpression expression;
        private readonly AppType valueType;
        private readonly Position position;

        public Position Position { get { return position; } }
        public IExpression Expression { get { return expression; } }
        public AppType ValueType { get { return valueType; } }

        public CheckTypeCondition(IExpression expression, AppType valueType, Position position)
        {
            this.expression = expression;
            this.valueType = valueType;
            this.position = position;
        }

        public void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class CallParameter
    {
        public CallParameter(IExpression expression)
        {
            this.expression = expression;
        }
        public IExpression expression;
    }


    public enum AppType
    {
        T_INT,
        T_FLOAT,
        T_STRING,
        T_IDENTIFIER,
        T_INT_TYPE,
        T_FLOAT_TYPE,
        T_STRING_TYPE,
        T_VOID_TYPE,
        T_EQUAL,
        T_NOT_EQUAL,
        T_GR_OR_EQ,
        T_GREATER,
        T_LO_OR_EQ,
        T_LOWER,
        T_VAR,
        T_ASSIGN,
        T_LPAREN,
        T_RPAREN,
        T_MULTIPLICATION,
        T_DIVISION,
        T_MODULO,
        T_PLUS,
        T_MINUS,
        T_IS,
        T_LBRACKET,
        T_RBRACKET,
        T_IF,
        T_ELIF,
        T_ELSE,
        T_CASE,
        T_WHILE,
        T_RETURN,
        T_SEMICOLON,
        T_UNKNOWN,
        T_COMMA,
        T_ETX,
        T_MATCH,
        T_COLON
    }
}
