﻿
using System;
using System.Collections.Generic;

namespace TKOM
{
    class Program
    {
        static void Main(string[] args)
        {
            var printer = new ConsolePrinter();
            if (args.Length < 1)
            {
                printer.Print("Program path not given");
                return;
            }

            List<string> programArgs = new();
            for(int i = 1; i<args.Length; i++)
            {
                programArgs.Add(args[i]);
            }

            var errorHandler = new ErrorHandler(printer);
            var argsSource = new StringSource(String.Join(", ", programArgs.ToArray()));
            ISource appSource;
            try
            {
                appSource = new FileSource(args[0]);
            }
            catch(Exception e)
            {
                if (e is System.IO.FileNotFoundException || e is System.IO.DirectoryNotFoundException)
                {
                    printer.Print("Can not find file");
                    return;
                }
                throw;
            }
            
            var embededFunctions = new List<IFunction>() { new PrintFunction() };

            try
            {
                var parsedArgs = new Parser(new Lexer(argsSource)).ParseArgs();
                var parsedProgram = new Parser(new Lexer(appSource)).ParseApp();
                var interpreter = new Interpreter(parsedArgs, parsedProgram, errorHandler, printer, embededFunctions);
                interpreter.Run();
            }
            catch (ReadableException e)
            {
                errorHandler.HandleException(e);
            }

        }
    }
}
