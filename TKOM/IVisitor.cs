﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKOM
{
    public interface IStatementVisitor
    {
        void Visit(IfStatement statement);
        void Visit(ElifStatement statement);
        void Visit(ElseStatement statement);
        void Visit(WhileStatement statement);
        void Visit(AssignStatement statement);
        void Visit(PatternMatchingStatement statement);
        void Visit(ReturnStatement statement);
        void Visit(CaseStatement statement);
        void Visit(FunCall statement);
    }

    public interface IExpressionVisitor
    {
        void Visit(AddOperationExpression expression);
        void Visit(SubOperationExpression expression);
        void Visit(MulOperationExpression expression);
        void Visit(DivOperationExpression expression);
        void Visit(ModOperationExpression expression);
        void Visit(NegateExpression expression);
        void Visit(StringExpression expression);
        void Visit(BracketExpression expression);
        void Visit(ValueRef expression);
        void Visit(FunCall expression);
        void Visit(CompareCondition expression);
        void Visit(CheckTypeCondition expression);
        void Visit(NumericExpression expression);
    }

    public interface IFunctionVisitor
    {
        void Visit(FunctionDefinition statement);
        void Visit(PrintFunction printFunction);
    }

}
