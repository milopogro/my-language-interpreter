﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKOM
{
    public class Interpreter : IStatementVisitor, IExpressionVisitor, IFunctionVisitor
    {
        private readonly Enviroment enviroment;
        private readonly App app;
        private readonly IList<CallParameter> args;
        private readonly IErrorHandler errorHandler;
        private readonly IPrinter printer;

        public Interpreter(IList<CallParameter> args, App app, IErrorHandler errorHandler, IPrinter printer, IList<IFunction> functions)
        {
            this.app = app;
            this.args = args;
            this.enviroment = new Enviroment();
            this.errorHandler = errorHandler;
            this.printer = printer;
            AddEmbededFunctions(functions);
        }

        private void AddEmbededFunctions(IList<IFunction> embededFunctions)
        {
            foreach (IFunction function in embededFunctions)
                app.FunctionDir.Add(function.Name, function);
        }

        public Variant Run()
        {
            try
            {
               RunMain();
            }
            catch (ReadableException e)
            {
                errorHandler.HandleException(e);
                enviroment.lastValue = new IntValue(-1);
            }
            if (enviroment.lastValue is null)
                enviroment.lastValue = new IntValue(-1);
            return enviroment.lastValue.ValueAsVariant;
        }

        public IList<IValue> GetValuesFromCallParameters(IList<CallParameter> callParameters)
        {
            IList<IValue> values = new List<IValue>();
            foreach (CallParameter cP in callParameters)
            {
                cP.expression.Accept(this);
                values.Add(enviroment.lastValue);
            }
            return values;
        }

        public void RunMain()
        {
            IFunction main;
            if (!app.FunctionDir.TryGetValue("main", out main))
                throw new NoMainException(app.Position);
            enviroment.PrepareToCallFun(GetValuesFromCallParameters(args), main.Parameters, app.Position);
            main.Accept(this);
        }

        public void Visit(FunCall funCall)
        {
            IFunction fun;
            if (!app.FunctionDir.TryGetValue(funCall.FunName, out fun))
                throw new FunctionNotDeclaredException(funCall.Position);
            enviroment.PrepareToCallFun(GetValuesFromCallParameters(funCall.ParameterList), fun.Parameters, funCall.Position);
            fun.Accept(this);
        }

        public void Visit(FunctionDefinition function)
        {
            int statementIndex = 0;
            enviroment.PushBackNewScope();
            while (!enviroment.IsReturnActive() && statementIndex<function.StatementList.Count)
            {
                function.StatementList[statementIndex].Accept(this);
                statementIndex++;
            }
            enviroment.PopFunctionCallContext();
            if (function.FunctionType != AppType.T_VOID_TYPE && !enviroment.IsReturnActive())
                throw new NoReturnException(function.Position);
            if(function.FunctionType == AppType.T_VOID_TYPE)
                enviroment.lastValue = new IntValue(-1);
            enviroment.DeactivateReturn();
            enviroment.lastValue = ConvertValue(function.FunctionType, enviroment.lastValue, function.Position);
        }

        private IValue ConvertValue(AppType valueType, IValue value, Position position)
        {
            switch (valueType)
            {
                case AppType.T_INT_TYPE:
                    return new IntValue(value.ValueAsInt);

                case AppType.T_FLOAT_TYPE:
                    return new FloatValue(value.ValueAsFloat);

                case AppType.T_STRING_TYPE:
                    return new StringValue(value.ValueAsString);

                case AppType.T_VOID_TYPE:
                    return new IntValue(value.ValueAsInt);

                default:
                    throw new IllegalOperationException(position);
            }
        }

        public void Visit(PrintFunction function)
        {
            printer.Print(enviroment.GetVariable("toPrint").ValueAsString);
            enviroment.PopFunctionCallContext();
        }

            public void Visit(IfStatement statement)
        {
            if (CheckCondition(statement.condition))
            {
                ExecuteStatementBlockWithNewScope(statement.block);
            }
            else
            {
                bool elifCompleted = false;
                int elifIndex = 0;
                while (!elifCompleted && elifIndex < statement.elifList.Count)
                {
                    if (CheckCondition(statement.elifList[elifIndex].condition))
                    {
                        statement.elifList[elifIndex].Accept(this);
                        elifCompleted = true;
                    }
                    elifIndex++;
                }
                if (!elifCompleted)
                {
                    if (statement.elseStatement is not null)
                    {
                        statement.elseStatement.Accept(this);
                    }

                }
            }
        }

        private void ExecuteStatementBlock(IList<IStatement> block)
        {
            int statementIndex = 0;
            while (!enviroment.IsReturnActive()&&statementIndex<block.Count)
            {
                block[statementIndex].Accept(this);
                statementIndex++;
            }
        }

        private void ExecuteStatementBlockWithNewScope(IList<IStatement> block)
        {
            enviroment.PushBackNewScope();
            ExecuteStatementBlock(block);
            enviroment.PopScope();
        }

        public bool CheckCondition(IExpression expression)
        {
            expression.Accept(this);
            if (enviroment.lastValue.ValueAsInt > 0)
                return true;
            return false;
        }

        public void Visit(ElifStatement elifStatement)
        {
            ExecuteStatementBlockWithNewScope(elifStatement.block);
        }

        public void Visit(ElseStatement elseStatement)
        {
            ExecuteStatementBlockWithNewScope(elseStatement.block);
        }

        public void Visit(WhileStatement whileStatement)
        {
            enviroment.PushBackNewScope();
            while (!enviroment.IsReturnActive() && CheckCondition(whileStatement.condition))
            {
                ExecuteStatementBlock(whileStatement.block);
            }
            enviroment.PopScope();
        }

        public void Visit(AssignStatement statement)
        {
            IValue currentValue = enviroment.GetVariable(statement.identifier);
            statement.rightValue.Accept(this);
            if (currentValue is null)
            {
                enviroment.lastValue.Mutable = statement.mutable;
                enviroment.RegisterVariable(statement.identifier, enviroment.lastValue);
            }
            else if (!statement.mutable)
                enviroment.EditVariable(statement.identifier, enviroment.lastValue, statement.Position);
            else
                throw new TryingToAddExistingVariableException(statement.Position);

        }

        public void Visit(PatternMatchingStatement patternStatement)
        {
            bool caseCompleted = false;
            int caseIndex = 0;

            enviroment.PushBackNewScope();
            patternStatement.expression.Accept(this);
            enviroment.RegisterVariable(patternStatement.identifier, enviroment.lastValue);
            while (!caseCompleted && caseIndex < patternStatement.caseList.Count)
            {
                if (CheckCondition(patternStatement.caseList[caseIndex].condition))
                {
                    patternStatement.caseList[caseIndex].Accept(this);
                    caseCompleted = true;
                }
                caseIndex++;
            }
            if (!caseCompleted)
            {
                if (patternStatement.elseStatement is not null)
                    patternStatement.elseStatement.Accept(this);
            }
        }

        public void Visit(ReturnStatement returnStatement)
        {
            returnStatement.result.Accept(this);
            enviroment.ActivateReturn();
        }

        public void Visit(CaseStatement caseStatement)
        {
            ExecuteStatementBlockWithNewScope(caseStatement.block);
        }

        private (IValue, IValue) GetBinaryOperationValues(IBinaryExpression expression)
        {
            expression.LeftExpression.Accept(this);
            var lVal = enviroment.lastValue;

            expression.RightExpression.Accept(this);
            var rVal = enviroment.lastValue;

            return (lVal, rVal);
        }

        public void Visit(AddOperationExpression expression)
        {
            var valueTuple = GetBinaryOperationValues(expression);
            valueTuple.Item1.Add(valueTuple.Item2);
            enviroment.lastValue = valueTuple.Item1.Result;
        }

        public void Visit(SubOperationExpression expression)
        {
            try
            {
                var valueTuple = GetBinaryOperationValues(expression);
                valueTuple.Item1.Sub(valueTuple.Item2);
                enviroment.lastValue = valueTuple.Item1.Result;
            }
            catch (IllegalValueOperationException e)
            {
                e.position = expression.Position;
                throw;
            }

        }

        public void Visit(MulOperationExpression expression)
        {
            try
            {
                var valueTuple = GetBinaryOperationValues(expression);
                valueTuple.Item1.Mul(valueTuple.Item2);
                enviroment.lastValue = valueTuple.Item1.Result;
            }
            catch (IllegalValueOperationException e)
            {
                e.position = expression.Position;
                throw;
            }
        }

        public void Visit(DivOperationExpression expression)
        {
            try
            {
                var valueTuple = GetBinaryOperationValues(expression);
                valueTuple.Item1.Div(valueTuple.Item2);
                enviroment.lastValue = valueTuple.Item1.Result;
            }
            catch (IllegalValueOperationException e)
            {
                e.position = expression.Position;
                throw;
            }
        }

        public void Visit(ModOperationExpression expression)
        {
            try
            {
                var valueTuple = GetBinaryOperationValues(expression);
                valueTuple.Item1.Mod(valueTuple.Item2);
                enviroment.lastValue = valueTuple.Item1.Result;
            }
            catch (IllegalValueOperationException e)
            {
                e.position = expression.Position;
                throw;
            }
        }

        public void Visit(NegateExpression expression)
        {
            try
            {
                expression.Expression.Accept(this);
                enviroment.lastValue.Negate();
                enviroment.lastValue = enviroment.lastValue.Result;
            }
            catch (IllegalValueOperationException e)
            {
                e.position = expression.Position;
                throw;
            }
        }

        public void Visit(StringExpression expression)
        {
            enviroment.lastValue = new StringValue(expression.Value);
        }

        public void Visit(BracketExpression expression)
        {
            expression.Expression.Accept(this);
        }

        public void Visit(ValueRef expression)
        {
            enviroment.lastValue = enviroment.GetVariable(expression.Name);
            if (enviroment.lastValue is null)
                throw new UndeclaredVariableException(expression.Position);
        }

        public void Visit(CompareCondition expression)
        {
            var valueTuple = GetBinaryOperationValues(expression);
            switch (expression.CompareOperation)
            {
                case AppType.T_EQUAL:
                    valueTuple.Item1.Equal(valueTuple.Item2);
                    break;

                case AppType.T_NOT_EQUAL:
                    valueTuple.Item1.NotEqual(valueTuple.Item2);
                    break;

                case AppType.T_LOWER:
                    valueTuple.Item1.Less(valueTuple.Item2);
                    break;

                case AppType.T_GREATER:
                    valueTuple.Item1.Greater(valueTuple.Item2);
                    break;

                case AppType.T_LO_OR_EQ:
                    valueTuple.Item1.LessEqual(valueTuple.Item2);
                    break;

                case AppType.T_GR_OR_EQ:
                    valueTuple.Item1.GreaterEqual(valueTuple.Item2);
                    break;
            }
            enviroment.lastValue = valueTuple.Item1.Result;
        }

        public void Visit(CheckTypeCondition expression)
        {
            bool checkType = false;
            expression.Expression.Accept(this);
            enviroment.lastValue.CheckType();
            switch (expression.ValueType)
            {
                case AppType.T_INT_TYPE:
                    checkType = (enviroment.lastValue.Result.ValueAsInt == 0);
                    break;

                case AppType.T_FLOAT_TYPE:
                    checkType = (enviroment.lastValue.Result.ValueAsInt == 1);
                    break;

                case AppType.T_STRING_TYPE:
                    checkType = (enviroment.lastValue.Result.ValueAsInt == 2);
                    break;
            }
            enviroment.lastValue = new IntValue(Convert.ToInt32(checkType));
        }

        public void Visit(NumericExpression expression)
        {
            if (expression.Value.GetValueType() == ValueType.INTTYPE)
                enviroment.lastValue = new IntValue(expression.Value.GetInt());
            else
                enviroment.lastValue = new FloatValue(expression.Value.GetFloat());

        }
    }
}
