﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKOM
{
    using Scope = Dictionary<string, IValue>;

    public class FunctionCallContext
    {
        public List<Scope> scopes = new List<Scope>();
        public int currentStatementIndex;
    }

    public class Enviroment
    {
        List<FunctionCallContext> callContextList = new List<FunctionCallContext>();
        public IValue lastValue;
        public IValue returnValue;
        bool returnActive;
        public bool IsReturnActive()
        { 
            return returnActive; 
        }

        public void DeactivateReturn() 
        { 
            returnActive = false;
        }

        public void ActivateReturn()
        {
            returnActive = true;
        }

        public void PrepareToCallFun(IList<IValue> values, IList<DefParameter> defParameters, Position position)
        {
            PushBackFunctionCallContext();
            PushBackNewScope(CallParamtersToScope(values, defParameters, position));
        }

        public void PushBackFunctionCallContext()
        { 
            callContextList.Add(new FunctionCallContext());
        }

        public void PopFunctionCallContext() 
        { 
            callContextList.RemoveAt(callContextList.Count - 1);
        }

        public void PushBackNewScope() 
        { 
            callContextList.Last().scopes.Add(new Dictionary<string, IValue>()); 
        }

        public void PushBackNewScope(Scope newScope)
        {
            callContextList.Last().scopes.Add(newScope);
        }

        public void PopScope() 
        { 
            callContextList.Last().scopes.RemoveAt(callContextList.Last().scopes.Count - 1);
        }

        public void RegisterVariable(string name, IValue value) 
        {
            callContextList.Last().scopes.Last().Add(name, value);
        }

        public void EditVariable(string name, IValue value, Position position)
        {
            var result = GetVariableWithScopeIndex(name);
            var currentValue = result.Item2;
            if (currentValue.Mutable)
            {
                value.Mutable = true;
                callContextList.Last().scopes[result.Item1][name] = value;
            }
            else
                throw new TryingToEditUnmutableVariableException(position);
            
        }

        public IValue GetVariable(string name) 
        {
            IValue value;
            bool ifContains = false;
            if (callContextList.Last().scopes.Count < 1)
                return null;

            int scopeIndex = 0;
            do
            {
                ifContains = callContextList.Last().scopes[scopeIndex].TryGetValue(name, out value);
                scopeIndex++;
            } while (!ifContains && scopeIndex < callContextList.Last().scopes.Count);

            if (!ifContains)
                return null;

            return value;
        }

        private (int, IValue) GetVariableWithScopeIndex(string name)
        {
            IValue value;
            bool ifContains = false;
            int scopeIndex = 0;
            do
            {
                ifContains = callContextList.Last().scopes[scopeIndex].TryGetValue(name, out value);
                scopeIndex++;
            } while (!ifContains && scopeIndex < callContextList.Last().scopes.Count);

            return (scopeIndex-1, value);
        }

        private static Scope CallParamtersToScope(IList<IValue> values, IList<DefParameter> defParameters, Position position)
        {
            if (values.Count != defParameters.Count)
                throw new WrongNumberOfParametersException(position);

            var newScope = new Scope();

            for (int i = 0; i < values.Count; i++)
            {
                var copy = values[i].Clone();
                copy.Mutable = defParameters[i].mutable;
                newScope.Add(defParameters[i].name, copy);
            }
            return newScope;
        }
    }


}

